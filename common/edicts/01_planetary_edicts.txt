##########################################################################
# Planet edicts
##########################################################################

# Variables:
#  name, the name of the edict, also serves as localisation key.
#  influence_cost, the base influence cost of activating the edict.
#  potential, planet or country trigger to decide whether the edict should be shown.
#  allow, planet or country trigger to decide if whether this edict can be activated.
#  effect, an effect that is executed when this trigger activates.
#  prerequisites, tech requirements for this trigger.
#  ai_will_do, determines AI scoring for edict
#  modifier, a list of modifiers
#  length, number of days the trigger is active.

# Sets the edict-cost for all edicts with this variable
@standardEdictCost = 150

planet_edict = {
	name = "reeducation_campaign"
	influence_cost = @standardEdictCost
	modifier = {
		pop_ethic_shift = -0.10
	}
	
	potential = {
		owner = {
			NOT = { #BO Changed
				OR = {
					#has_ethic = ethic_fanatic_collectivist #Why would fanatic collectivists be the ones who couldn't re-educate?
					has_ethic = ethic_fanatic_individualist
					#has_policy_flag = propaganda_none
				}
			}
		}
	}
	
	length = 3600
	
	allow = {
	}
	
	ai_weight = {
		weight = 1
		modifier = {
			factor = 0
			num_pops < 10
		}
	}	
}

#planet_edict = {
#	name = "propaganda"
#	influence_cost = @standardEdictCost
#	length = 3600
#	modifier = {
#		pop_happiness = 0.10
#	}
#	
#	prerequisites = { 
#		"tech_planetary_unification"
#	}
#	
#	allow = {
#	}
#	
#	ai_weight = {
#		weight = 1
#		modifier = {
#			factor = 0
#			num_pops < 10
#		}		
#	}	
#}

planet_edict = {
	name = "land_of_opportunity"
	influence_cost = @standardEdictCost
	modifier = {
		planet_migration_all_pull = 1
	}
	
	length = 3600
	
	allow = {
	}
	
	ai_weight = {
		weight = 0
	}	
}

#planet_edict = {
#	name = "capacity_overload"
#	influence_cost = @standardEdictCost
#	modifier = {
#		tile_resource_energy_mult = 0.15
#	}
#	
#	potential = {
#		owner = {
#			NOT = {
#				has_ethic = ethic_fanatic_individualist
#			}
#		}
#	}
#	
#	length = 3600
#	
#	allow = {
#	}
#	
#	ai_weight = {
#		weight = 1
#		modifier = {
#			factor = 0
#			num_pops < 10
#		}		
#	}	
#	
#	prerequisites = {
#		"tech_power_hub_1"		
#	}
#}

#planet_edict = {
#	name = "production_targets"
#	influence_cost = @standardEdictCost
#	modifier = {
#		tile_resource_minerals_mult = 0.15
#	}
#	
#	length = 3600
#	
#	prerequisites = {
#		"tech_colonial_centralization"
#	}
#	
#	allow = {
#	}
#	
#	ai_weight = {
#		weight = 1
#		modifier = {
#			factor = 0
#			num_pops < 10
#		}
#	}	
#}

#planet_edict = {
#	name = "infrastructure_projects"
#	influence_cost = @standardEdictCost
#	modifier = {
#		planet_building_cost_mult = -0.25
#		building_time_mult = -0.25
#	}
#	
#	length = 3600
#	
#	allow = {
#	}
#	
#	ai_weight = {
#		weight = 1
#		modifier = {
#			factor = 0
#			NOT = {
#				any_tile = {
#					has_blocker = no 
#					has_building = no
#				}
#			}
#		}
#	}	
#}


##########################################################################
# FANATIC ETHICS PLANETARY EDICTS
##########################################################################

#Didn't feel like commenting all these out so I just deleted them.