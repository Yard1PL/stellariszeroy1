democratic_tradition = {
	potential = {
		has_government = gov_democracy
	}
	allow = {
		always = no #Cannot be changed manually
	}
	option = {
		name = "democratic_tradition_05" #Very Good
		policy_flags = {
			democratic_tradition_05
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = democratic_tradition_05
			}
		}
		pop_happiness = {
			base = 0.06
		}
	}
	option = {
		name = "democratic_tradition_04" #Fairly Good
		policy_flags = {
			democratic_tradition_04
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				OR = {
					NOT = {
						OR = {
							has_policy_flag = democratic_tradition_05
							has_policy_flag = democratic_tradition_04
							has_policy_flag = democratic_tradition_03
							has_policy_flag = democratic_tradition_02
							has_policy_flag = democratic_tradition_01
						}
					}
					has_policy_flag = democratic_tradition_04
				}
			}
		}
		pop_happiness = {
			base = 0.03
		}
	}
	option = {
		name = "democratic_tradition_03" #Ok
		policy_flags = {
			democratic_tradition_03
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = democratic_tradition_03
			}
		}
		pop_happiness = {
			base = 0
		}
	}
	option = {
		name = "democratic_tradition_02" #Poor
		policy_flags = {
			democratic_tradition_02
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = democratic_tradition_02
			}
		}
		pop_happiness = {
			base = -0.05
		}
	}
	option = {
		name = "democratic_tradition_01" #Terrible
		policy_flags = {
			democratic_tradition_01
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = democratic_tradition_01
			}
		}
		pop_happiness = {
			modifier = {
				add = -0.10
			}
		}
	}
}

authority = {
	potential = {
		always = yes
	}
	allow = {
		always = no #Cannot be changed manually
	}
	option = {
		name = "authority_05" #Resources at the cost of revolution risk
		policy_flags = {
			authority_05
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = authority_05
			}
		}
		modifier = {
			tile_resource_energy_mult = 0.16
			tile_resource_minerals_mult = 0.20
			country_resource_influence_add = -2
		}
	}
	option = {
		name = "authority_04" #Resources at the cost of revolution risk
		policy_flags = {
			authority_04
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = authority_04
			}
		}
		modifier = {
			tile_resource_energy_mult = 0.08
			tile_resource_minerals_mult = 0.10
			country_resource_influence_add = -1
		}
	}
	option = {
		name = "authority_03" #Stable
		policy_flags = {
			authority_03
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				OR = {
					NOT = {
						OR = { #Defaults to this level
							has_policy_flag = authority_05
							has_policy_flag = authority_04
							has_policy_flag = authority_03
							has_policy_flag = authority_02
							has_policy_flag = authority_01
						}
					}
					has_policy_flag = authority_03
				}
			}
		}
	}
	option = {
		name = "authority_02" #Growth at the cost of separatists risk
		policy_flags = {
			authority_02
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = authority_02
			}
		}
		modifier = {
			tile_resource_energy_mult = -0.05
			tile_resource_minerals_mult = -0.10
			country_resource_influence_add = 1
		}
	}
	option = {
		name = "authority_01" #Growth at the cost of separatists risk
		policy_flags = {
			authority_01
		}
		valid = {
			custom_tooltip = {
				text = "CANNOT_BE_SET_MANUALLY"
				has_policy_flag = authority_01
			}
		}
		modifier = {
			tile_resource_energy_mult = -0.10
			tile_resource_minerals_mult = -0.20
			country_resource_influence_add = 2
		}
	}
}