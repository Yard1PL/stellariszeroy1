opinion_declared_legitimate = {
	opinion = {
		base = 25	
	}
}

opinion_declared_illegitimate = {
	opinion = {
		base = -25	
	}
}

opinion_accepted_outlaws = {
	opinion = {
		base = -10
	}
	
	accumulative = yes
	
	min = -60
	
	decay = {
		base = 1
	}
}

opinion_supported_smugglers = {
	opinion = {
		base = -5
	}
	
	accumulative = yes
	
	min = -150
	
	decay = {
		base = 1
	}
}