#These factions appear in democratic nations. They will try to influence the national ethics in their favor and will become rebellious if ignored.

@primary_ethic = ethic_individualist
@primary_fanatic_ethic = ethic_fanatic_individualist
@opposed_ethic = ethic_collectivist
@opposed_fanatic_ethic = ethic_fanatic_collectivist

#Copy from here down (remember to replace!)
@influence_tier1 = 50
@influence_tier2 = 150
@influence_tier3 = 300
@base_attraction = 0
@militarised_attraction = -100
@minor_ethic_attraction = 40
@fanatic_ethic_attraction = 50
@happiness_attraction = 80
@dangerous_support = 0.85
@promote_cost = 50
@promote_support = 0.1
@promote_support_change = -0.1
@is_content_base = 10
@is_content_has_ethic = 25
@is_content_has_fanatic_ethic = 50
@is_content_rejected_reform_factor = 0.2

@militarise_spawn_chance = 5
@militarise_minor_opposed_ethic_chance = 5
@militarise_fanatic_opposed_ethic_chance = 10
@militarise_support_change = -0.1
@militarise_democracy_factor = 0.5
@banned_support_change = -0.4
@militarise_banned_chance = 10

@leader_skill_level_0 = 0.9
@leader_skill_level_1 = 1
@leader_skill_level_2 = 1.1
@leader_skill_level_3 = 1.2
@leader_skill_level_4 = 1.3
@leader_skill_level_5 = 1.4

@leader_trait_champion_of_the_people = 1.1
@leader_trait_charismatic = 1.1

@scandal_small_factor = 0.8
@scandal_big_factor = 0.7

individualist_party = {

	is_potential = {
		OR = {
			has_government = gov_democracy
			#Room for more!
		}	
		NOT = { has_faction = individualist_party }
	}	

	valid = {
		root.owner = {
			count_pop_factions = {
				limit = { is_pop_faction_type = individualist_party }
				count < 2
			}
		}
	}
	
	parameters = {
		empire = {
			type = country
			valid_objects = {
				is_same_value = root
			}
		}
	}

	on_create = {
		set_name = random
	}

	is_dangerous = {
		support > @dangerous_support
		OR = {
			has_pop_faction_flag = rejected_reform
			has_pop_faction_flag = party_banned
			AND = {
				has_pop_faction_flag = rejected_power_split_change
				NOT = { root.owner = { has_policy_flag = executive_legislative_power_split_pro_legislative } }
			}
		}
		NOT = {
			root.owner = {
				AND = {
				exists = ruler
				ruler = {
					exists = pop_faction
					pop_faction = {
						is_same_value = root
					}
				}
			}
		}
	}
	}

	attraction = {
		base = @base_attraction

		modifier = {
			add = @minor_ethic_attraction
			pop_has_ethic = @primary_ethic
		}

		modifier = {
			add = @fanatic_ethic_attraction
			pop_has_ethic = @primary_fanatic_ethic
		}

		modifier = {
			add = @militarised_attraction
			has_modifier = "pop_rebellious"
		}

		scaled_modifier = {
			scope = this
			add = @happiness_attraction
			calc = pop_happiness
		}

#		scaled_modifier = {
#			scope = this
#			add = 1000
#			calc = pop_ethic_shift
#		}

		modifier = {
			factor = 1.2
			parameter:empire = {
				has_ethic = @opposed_ethic
			}
		}

		modifier = {
			factor = 1.5
			parameter:empire = {
				has_ethic = @opposed_fanatic_ethic
			}
		}

		modifier = { #party specific
			factor = 1.1 #If nothing else is causing a dispute then indie-vs-collect will be the main focus
		}
		
		modifier = {
			factor = 1.1
			has_pop_flag = individualist_party_promoted
		}		

		modifier = {
			factor = @scandal_small_factor
			owner.from = { has_pop_faction_flag = scandal_small }
		}

		modifier = {
			factor = @scandal_big_factor
			owner.from = { has_pop_faction_flag = scandal_big }
		}

		modifier = {
			add = -100
			OR = {
				pop_has_ethic = @opposed_ethic
				pop_has_ethic = @opposed_fanatic_ethic
			}
		}

		modifier = {
			factor = 0
			OR = {
				happiness < 0.30
			has_pop_flag = "political_apathy"
				is_slavebot = yes
			}
		}

	}

	mandate = { #Adjust mandates per party!
		key = "individualist_mandate"
		parameter_transfer = {}
		weight = {
			base = 100
		}
	}

	on_set_leader = { # I copied this from the planet separatists file. It should work fine but revisions might be worth considering.
		generate_party_leader = yes
	}

	demand = {
		title = "POP_FACTION_INDIVIDUALIST_PARTY_DEMAND_TITLE"
		desc = "POP_FACTION_INDIVIDUALIST_PARTY_DEMAND_DESC"
	}

	actions = {
		promote_party = {
			title = "POP_FACTION_PARTY_PROMOTE_PARTY_TITLE"
			description = "POP_FACTION_PARTY_PROMOTE_PARTY_DESC"

			potential = {
				NOT = { has_pop_faction_flag = party_banned }
			}

			cost = {
				influence = @influence_tier2
			}

			valid = {
				NOT = {
					custom_tooltip = {
						text = "PROMOTE_PARTY_RECENT"
						has_pop_faction_flag = recently_promoted
					}
				}
			}

			effect = {
				add_support = 0.2
				set_timed_pop_faction_flag = { flag = recently_promoted days = 730 }
			}

			ai_weight = {
				base = 0
				modifier = {
					add = 2
					root.owner = {
						custom_tooltip = {
							text = "POP_FACTION_GENERIC_LEADER_IS_RULER"
							exists = ruler
							ruler = {
								exists = pop_faction
								pop_faction = {
									is_same_value = root
								}
							}
						}
					}
				}
				modifier = {
					factor = 2
					root.owner = {
						NOT = { has_country_flag = individualist_party_parliamentary_majority }
					}
				}
				modifier = {
					factor = 2
					root.owner = {
						has_policy_flag = state_press
					}
				}
			}
		}
		ban_party = {
			title = "POP_FACTION_PARTY_BAN_PARTY_TITLE"
			description = "POP_FACTION_PARTY_BAN_PARTY_DESC"

			cost = {
				influence = @influence_tier3
			}
			
			potential = {
				NOT = { has_pop_faction_flag = party_banned }
			}			

			valid = {
				root.owner = {
					OR = {
						has_policy_flag = authority_05
						has_policy_flag = authority_04
						has_policy_flag = authority_03
					}
					NOT = {
						OR = {
							has_policy_flag = democratic_tradition_05
							has_policy_flag = democratic_tradition_04
							has_policy_flag = executive_legislative_power_split_pro_legislative
						}
						custom_tooltip = {
							text = "POP_FACTION_GENERIC_LEADER_IS_RULER"
							exists = ruler
							ruler = {
								exists = pop_faction
								pop_faction = {
									is_same_value = root
								}
							}
						}
					}
					OR = {
						NOT = {
							custom_tooltip = {
								text = "POP_FACTION_NOT_INDIVIDUALIST_PARTY_MAJORITY"
								 has_country_flag = individualist_party_parliamentary_majority
							}
						}
						has_policy_flag = executive_legislative_power_split_executive_domination
						has_policy_flag = executive_legislative_power_split_pro_executive
					}
				}
			}

			effect = {
				custom_tooltip = DEMOCRATIC_TRADITION_DECREASED
				hidden_effect = {
					set_pop_faction_flag = party_banned
					set_pop_faction_flag = rejected_reform
					root.owner = {
						decrease_democratic_tradition = yes
						count_parilamentary_election_votes = yes
						log = "[Root.GetName] in [This.GetName] got benned"										
					}
				}
				add_support = @banned_support_change
			}

			ai_weight = {
				base = 1
				modifier = {
					add = 9
					root.owner = {
						has_policy_flag = democratic_tradition_02
					}
				}
				modifier = {
					add = 19
					root.owner = {
						has_policy_flag = democratic_tradition_01
					}
				}
				modifier = {
					factor = 0
					root.owner = {
						OR = {
							has_ethic = @primary_ethic
							has_ethic = @primary_fanatic_ethic
						}
					}
				}
				modifier = {
					factor = 2
					root.owner = {
						OR = {
							has_ethic = @opposed_ethic
							has_ethic = @opposed_fanatic_ethic
						}
					}
				}
				modifier = {
					factor = 1.5
					root.owner = {
						OR = {
							has_country_flag = collectivist_party_parliamentary_majority #Adjust per party!
							AND = {
								exists = ruler
								ruler = {
									exists = pop_faction
									pop_faction = {
										is_pop_faction_type = collectivist_party
									}
								}
							}
						}
					}
				}
			}
		}
		unban_party = {
			title = "POP_FACTION_PARTY_UNBAN_PARTY_TITLE"
			description = "POP_FACTION_PARTY_UNBAN_PARTY_DESC"

			cost = {
				influence = @influence_tier3
			}

			potential = {
				has_pop_faction_flag = party_banned
			}

			valid = {
				root.owner = {
					NOT = {
						OR = {
							has_policy_flag = democratic_tradition_02
							has_policy_flag = democratic_tradition_01
							has_policy_flag = executive_legislative_power_split_balanced
							has_policy_flag = executive_legislative_power_split_pro_legislative
						}
					}
					OR = {
						NOT = {
							custom_tooltip = {
								text = "POP_FACTION_collectivist_PARTY_MAJORITY"
								 has_country_flag = collectivist_party_parliamentary_majority
							}
						}
						has_policy_flag = executive_legislative_power_split_executive_domination
						has_policy_flag = executive_legislative_power_split_pro_executive
					}
				}
			}

			effect = {
				custom_tooltip = DEMOCRATIC_TRADITION_INCREASED
				hidden_effect = {
					remove_pop_faction_flag = party_banned
					remove_pop_faction_flag = rejected_reform
					root.owner = {
						increase_democratic_tradition = yes
					}
				}
			}

			ai_weight = {
				base = 0
				modifier = {
					add = 1
					root.owner = {
						OR = {
							has_ethic = @primary_ethic
							has_ethic = @primary_fanatic_ethic
						}
					}
				}
				modifier = {
					add = 1
					support > 0.84
				}
				modifier = {
					add = 9
					root.owner = {
						has_policy_flag = democratic_tradition_04
					}
				}
				modifier = {
					add = 19
					root.owner = {
						has_policy_flag = democratic_tradition_05
					}
				}
				modifier = {
					factor = 0
					root.owner = {
						OR = {
							has_ethic = @opposed_ethic
							has_ethic = @opposed_fanatic_ethic
						}
					}
				}
				modifier = {
					factor = 4
					root.owner = {
						OR = {
							has_ethic = @primary_ethic
							has_ethic = @primary_fanatic_ethic
						}
					}
				}
				modifier = {
					factor = 0.2
					root.owner = {
						OR = {
							has_country_flag = collectivist_party_parliamentary_majority #Adjust per party!
AND = {
							exists = ruler
							ruler = {
								exists = pop_faction
								pop_faction = {
									is_pop_faction_type = collectivist_party
}
								}
							}
						}
					}
				}
			}
		}
	}

	support_effects = {

		promote_party = {
			title = "POLITICAL_PARTY_PROMOTE"
			min_support = 0.1
			max_support = 1.0

			spawn_chance = {
				base = 2
				modifier = {
					add = 2
					support > 0.2
				}
				modifier = {
					add = 2
					support > 0.4
				}
				modifier = {
					add = 2
					support > 0.6
				}
				modifier = {
					add = 2
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 1.5
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 1.5
					root.owner = {
						has_policy_flag = state_press
					}
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 0
					root.owner = {
						has_policy_flag = state_press
					}
					NOT = { has_pop_faction_flag = recently_promoted }
				}				
				modifier = {
					factor = 1.1
					NOT = {
						custom_tooltip = {
							text = "POP_FACTION_NOT_INDIVIDUALIST_PARTY_MAJORITY"
							root.owner = { has_country_flag = individualist_party_parliamentary_majority }
						}
					}
				}
				modifier = {
					factor = 1.2
					custom_tooltip = {
						text = "POP_FACTION_COLLECTIVIST_PARTY_MAJORITY"
						root.owner = { has_country_flag = collectivist_party_parliamentary_majority }
					}
				}
				modifier = {
					factor = 1.1
					root.owner = {
						has_ethic = @opposed_ethic
					}
				}
				modifier = {
					factor = 1.2
					root.owner = {
						has_ethic = @opposed_fanatic_ethic
					}
				}
				modifier = {
					factor = 0.5
					custom_tooltip = {
						text = "POP_FACTION_PARTY_BANNED"
						has_pop_faction_flag = party_banned
					}
				}				
			}


			effect = {
				add_support = -0.05
				custom_tooltip = "POLITICAL_PARTY_PROMOTE_EFFECT"
				hidden_effect = {
					every_owned_pop = { #If this works right then every pop in the faction has a chance of influencing another random pop on the planet or in the country
						random_list = {
							10 = {
								parameter:empire = {
									random_owned_pop = {
										limit = { is_slavebot = no }
										set_timed_pop_flag = { flag = individualist_party_promoted days = 365 }
									}
								}
							}
							30 = {
								planet = {
									random_pop	= {
										limit = { is_slavebot = no }
										set_timed_pop_flag = { flag = individualist_party_promoted days = 365 }
									}
								}
							}
							60 = {
								#Do nothing
							}
						}
					}
				}
			}
		}
		
		# Gather Support
		gather_support = {
			title = "POLITICAL_PARTY_GATHER_SUPPORT"
			min_support = 0.2
			max_support = 1.0

			spawn_chance = {
				base = 0
				modifier = {
					add = 2
					support > 0.2
				}
				modifier = {
					add = 2
					support > 0.4
				}
				modifier = {
					add = 2
					support > 0.6
				}
				modifier = {
					add = 2
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 1.5
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 1.5
					root.owner = {
						has_policy_flag = state_press
					}
					has_pop_faction_flag = recently_promoted
				}
				modifier = {
					factor = 0
					root.owner = {
						has_policy_flag = state_press
					}
					NOT = { has_pop_faction_flag = recently_promoted }
				}
				modifier = {
					factor = @is_content_rejected_reform_factor
					custom_tooltip = {
						text = "POP_FACTION_RECENTLY_REJECTED_REFORM"
						has_pop_faction_flag = rejected_reform
					}
				}
			}

			effect = {
				add_support = @promote_support_change
				custom_tooltip = "POLITICAL_PARTY_GATHER_SUPPORT_EFFECT"
				hidden_effect = {
					every_owned_pop = { #If this works right then every pop in the faction has a chance of influencing another random pop on the planet or in the country
						random_list = {
							20 = {
								parameter:empire = {
									random_owned_pop = {
										limit = { is_slavebot = no }
										make_pop_more_individualist = yes #Adjust Per Party
									}
								}
							}
							40 = {
								planet = {
									random_pop	= {
										limit = { is_slavebot = no }
										make_pop_more_individualist = yes #Adjust Per Party
									}
								}
							}
							40 = {
								#Do nothing
							}
						}
					}
				}
			}
		}

		demand_reform = {
			title = "POLITICAL_PARTY_DEMAND_REFORM"
			min_support = 0.1
			max_support = 1.0

			spawn_chance = {
				base = 0
				
				modifier = {
					add = 1
					custom_tooltip = {
						text = "POP_FACTION_PARTY_CAN_CHANGE_POLICY"
						AND = {
							root.owner = { individualist_can_change_policy = yes }
							NOT = { has_pop_faction_flag = rejected_policy_change }
						}
					}
				}
				
				modifier = {
					add = 4
					custom_tooltip = {
						text = "POP_FACTION_PARTY_CAN_CHANGE_POWER_SPLIT"
						root.owner = {	
							individualist_can_change_power_split_angry = yes
						}
					}
				}				

				modifier = {
					add = 1
					custom_tooltip = {
						text = "POP_FACTION_PARTY_CAN_CHANGE_POWER_SPLIT"
						root.owner = {	
							individualist_can_change_power_split_not_angry = yes
						}
					}
				}
				
				modifier = {
					add = 2
					custom_tooltip = {
						text = "POP_FACTION_PARTY_CAN_CHANGE_ETHIC"
						root.owner = {	
							AND = {
								has_country_flag = individualist_party_parliamentary_majority
								root = { support > 0.39 }
								NOT = {
									root = {
										has_pop_faction_flag = rejected_reform
									}
								}
								NOT = {	has_ethic = @primary_ethic }
								NOT = {	has_ethic = @primary_fanatic_ethic }
							}
						}
					}
				}

				modifier = {
					add = 1
					custom_tooltip = {
						text = "POP_FACTION_PARTY_CAN_CHANGE_ETHIC"
						root.owner = {	
							AND = {
								has_country_flag = individualist_party_parliamentary_majority
								root = { support > 0.39 }
								NOT = {
									root = {
										has_pop_faction_flag = rejected_reform
									}
								}
								has_ethic = @primary_ethic
								NOT = {	has_ethic = @primary_fanatic_ethic }
							}
						}
					}
				}													
				
				modifier = {
					factor = 1.5
					support > 0.39
				}
				modifier = {
					factor = 1.5
					support > 0.6
				}
				modifier = {
					factor = 1.5
					custom_tooltip = {
						text = "POP_FACTION_GENERIC_LEADER_IS_RULER"
						owner = {
							exists = ruler
							ruler = {
								exists = pop_faction
								pop_faction = {
									is_same_value = root
								}
							}
						}
					}
				}
				modifier = {
					factor = 1.5
					root.owner = { has_country_flag = individualist_party_parliamentary_majority }
				}
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POP_FACTION_PARTY_BANNED"
						has_pop_faction_flag = party_banned
					}
				}
			}

			effect = {
				custom_tooltip = "POLITICAL_PARTY_ATTEMPT_REFORM_EFFECT"
				hidden_effect = {
					root = {
						random_list = {
							12 = {
								modifier = {
									factor = 0
									OR = {
										owner = { individualist_can_change_policy = no }
										has_pop_faction_flag = rejected_policy_change
									}
								}
								pop_faction_event = { #Demand policy change
									id = bo_parliament.1
								}
							}
							1 = {
								modifier = {
									add = 2
									AND = { 
										root.owner = {
											ruler_party_has_majority = no
											ruler = {
												AND = {
													exists = pop_faction
													AND = {
														pop_faction = { NOT = { is_same_value = root } }
														owner = { has_policy_flag = executive_legislative_power_split_executive_domination }											
													}
												}													
											}
										}
									}
								}
								modifier = {
									add = 4
									AND = { 
										root.owner = {
											ruler_party_has_majority = no
											exists = ruler
											ruler = {
												has_leader_flag = angered_individualist_party														
											}
										}
									}
								}								
								modifier = {
									factor = 1.5
									support > 0.39
								}
								modifier = {
									factor = 2
									support > 0.6
								}
								modifier = {
									factor = 0
									OR = {							
										root.owner = {
											individualist_can_change_power_split_angry = no
										}
									}
								}
								pop_faction_event = { #Demand policy change
									id = bo_parliament.11
								}
							}
							1 = {
								modifier = {
									add = 2
									support > 0.39
								}
								modifier = {
									add = 3
									support > 0.6
								}
								modifier = {
									factor = 1.5
									root.owner = { has_ethic = @opposed_ethic }
								}
								modifier = {
									factor = 3
									root.owner = { has_ethic = @opposed_fanatic_ethic }
								}
								modifier = {
									factor = 2
									owner = {
										exists = ruler
										ruler = {
										exists = pop_faction
										pop_faction = {
											is_same_value = root
											}
										}
									}
								}
								modifier = {
									factor = 0
									OR = {
										support < 0.40
										has_pop_faction_flag = rejected_reform
										root.owner = { has_ethic = @primary_fanatic_ethic }
										root.owner = { NOT = { has_country_flag = individualist_party_parliamentary_majority } }
									}
								}
								pop_faction_event = { #Demand ethics change
									id = bo_faction.1
								}
							}
						}
					}
				}
			}
		}

		militarise = { #Dissatisfied parties will prepare their members for WAR!
			title = "POLITICAL_PARTY_MILITARISE"
			min_support = 0.85
			max_support = 1.0
			spawn_chance = {
				base = @militarise_spawn_chance
				modifier = {
					add = @militarise_minor_opposed_ethic_chance
					root.owner = {
						has_ethic = @opposed_ethic
					}
				}
				modifier = {
					add = @militarise_fanatic_opposed_ethic_chance
					root.owner = {
						has_ethic = @opposed_fanatic_ethic
					}
				}
				modifier = {
					factor = @militarise_banned_chance
					custom_tooltip = {
						text = "POP_FACTION_PARTY_BANNED"
						has_pop_faction_flag = party_banned
					}
				}
#				modifier = {
#					factor = @militarise_democracy_factor
#					owner = {
#						has_government = gov_democracy
#					}
#				}
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POP_FACTION_GENERIC_LEADER_IS_RULER"
						owner = {
							exists = ruler
							ruler = {
								exists = pop_faction
								pop_faction = {
									is_same_value = root
								}
							}
						}
					}
				}
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POLITICAL_PARTY_MILITARISE_REJECTED_REFORM"
						NOT = {
							has_pop_faction_flag = party_banned
							has_pop_faction_flag = rejected_reform
							AND = {
							has_pop_faction_flag = rejected_power_split_change
								NOT = { owner = { has_policy_flag = executive_legislative_power_split_pro_legislative } }
							}
						}
					}
				}
			}
			effect = {
				add_support = @militarise_support_change
				custom_tooltip = "POLITICAL_PARTY_MILITARISE_EFFECT"
				hidden_effect = {
					root = {
						pop_faction_event = {
							id = bo_faction.2
						}
					}
				}
			}
		}

	}
}
