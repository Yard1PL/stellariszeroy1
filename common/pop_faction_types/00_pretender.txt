
@install_pretender_cost = 100

pretender = {
	lose_support_on_pop_leave = no

	is_potential = {
		always = no
	}
	
	parameters = {
		pretender = {
			type = leader
		}
		
		empire = {
			type = country
			valid_objects = {
				is_same_value = root
			}
		}
	}

	can_create = {
		always = yes
	}

	is_dangerous = {
		always = no
	}

	flag = {
		icon = {
			category = "ornate"
			file = "flag_ornate_11.dds"
		}
		background= {
			category = "backgrounds"
			file = "circle.dds"
		}
		colors={
			"dark_teal"
			"teal"
			"null"
			"null"
		}
	}
	
	on_create = {
		while = {
			limit = {
				owner = {
					count_owned_pops = {
						limit = {
							exists = pop_faction
							pop_faction = { is_same_value = root }
						}
						count < 5
					}
				}
			}
			random_owned_pop = {
				set_pop_faction = root
			}
		}
		
		set_name = random
	}
	
	on_set_leader = {
		assign_leader = parameter:pretender
	}
	
	demand = {
		title = "POP_FACTION_PRETENDER_DEMAND_TITLE"
		desc = "POP_FACTION_PRETENDER_DEMAND_DESC"
	}
	
	# Executed with a pop scope, country is stored in root
	attraction = {
		base = 0

		modifier = {
			add = 85
			exists = pop_faction
			pop_faction = {
				exists = from
				is_same_value = from
			}
		}

		modifier = {
			factor = 0
			is_slavebot = yes
		}
	}
	
	actions = {
		install_new_ruler = {
			title = "POP_FACTION_PRETENDER_INSTALL_BO_TITLE"
			description = "POP_FACTION_PRETENDER_INSTALL_BO_DESC"
			
			cost = {
				influence = @install_pretender_cost
			}
			
			effect = {
				owner = {
					hidden_effect = {
						every_owned_pop = {
							limit = {
								NOT = {
									AND = {
										exists = pop_faction
										pop_faction = { is_same_value = root }
									}
								}
							}
							add_modifier = {
								modifier = pop_dislikes_pretender
								days = 3600
							}
						}
					}
					remove_modifier = troubled_succession
					kill_leader = {
						type = ruler
						heir = yes
						show_notification = no
					}
					set_leader = parameter:pretender
					every_pop_faction = {
						limit = {
							is_pop_faction_type = "pretender"
						}
						kill_pop_faction = yes
					}
				}
			}
			
			ai_weight = {
				base = 0
#				modifier = {
#					add = 1
#					support > 0.79
#				}
			}
		}
	}
	
	support_effects = {
		civil_disobedience = {
			title = CIVIL_DISOBEDIENCE
			min_support = 0.0
			max_support = 1.0
			
			spawn_chance = {
				base = 100
				modifier = {
					factor = 0
					parameter:empire = {
						has_edict = martial_law
					}
				}
			}
			
			effect = {
				custom_tooltip = CIVIL_DISOBEDIENCE_EFFECT
				hidden_effect = {
					every_owned_pop = {
						add_modifier = {
							modifier = pop_on_strike
							days = 30
						}
					}
				}
			}
		}
		
		attrition = {
			title = "POP_FACTION_PRETENDER_ATTRITION_TITLE"
			min_support = 0.1
			max_support = 1.0
			
			spawn_chance = {
				value = 100
			}
			
			effect = {
				custom_tooltip = "POP_FACTION_PRETENDER_ATTRITION_DESC"
				hidden_effect = {
					random_owned_pop = {
						set_pop_faction = owner.default_pop_faction
					}
				}
				add_support = -0.1
			}
		}
	}
}