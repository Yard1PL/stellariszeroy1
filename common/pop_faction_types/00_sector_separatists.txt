@leader_skill_level_0 = 0.7
@leader_skill_level_1 = 0.8
@leader_skill_level_2 = 0.9
@leader_skill_level_3 = 1
@leader_skill_level_4 = 1.1
@leader_skill_level_5 = 1.2

@leader_trait_champion_of_the_people = 1.1
@leader_trait_charismatic = 1.1

sector_separatists = { #sector_separatists
	
	is_potential = {
		sectors > 0 		
	}
	
	parameters = {
		sector = {
			type = sector
			#filter = owned
			valid_objects = {
				is_core_sector = no
				owner = {
					is_same_value = root
				}
			}
		}
		owner_empire = {
			type = country
			valid_objects = {
				is_same_value = root
			}
		}
	}
	
	valid = {
		parameter:sector = { debug_scope_type = sector
			owner = {
				is_same_value = root.owner
			}
		}		
	}

	is_dangerous = {
		support > 0.49
		any_support_effect_chance > 0.01
		owner = {
			exists = ruler
			ruler = {
				NOT = { is_same_value = root.leader }
			}
		}
	}
	
	attraction = {
		base = 35
		
		modifier = {
			add = 9
			parameter:owner_empire = {
				has_policy_flag = authority_01
			}
		}
		
		modifier = {
			add = 4
			parameter:owner_empire = {
				has_policy_flag = authority_02
			}
		}
		
		modifier = {
			add = -2
			parameter:owner_empire = {
				has_policy_flag = authority_04
			}
		}
		
		modifier = {
			add = -4
			parameter:owner_empire = {
				has_policy_flag = authority_05
			}
		}
		
		modifier = {
			add = 4
			parameter:owner_empire = { has_modifier = recent_succession }
		}
		
		modifier = { #moved_by_protest gives a -5 happiness penalty for a total of +10 relative attraction to rebels.
			add = 5
			has_modifier = moved_by_protest
		}
		
		modifier = {
			add = 4
			opposing_ethics_divergence = {
				steps = 1
				who = parameter:planet
			}
		}
		
		modifier = {
			add = 8
			opposing_ethics_divergence = {
				steps = 2
				who = parameter:planet
			}
		}

		modifier = {
			add = 10
			OR = {
				pop_has_ethic = ethic_xenophobe
				pop_has_ethic = ethic_fanatic_xenophobe
			}
			NOT = {
				is_same_species = parameter:owner_empire
			}
		}
		
		modifier = {
			add = 10
			parameter:owner_empire = {
				OR = {
					has_ethic = ethic_xenophobe
					has_ethic = ethic_fanatic_xenophobe
				}
			}
			NOT = {
				is_same_species = parameter:owner_empire
			}
		}

		modifier = {
			factor = 1.2
			planet = {
				exists = sector
				sector = {
					is_same_value = parameter:sector
				}
				distance = {
					source = parameter:owner_empire.capital_scope
					min_distance = 50 #75
				}
			}
		}

		modifier = {
			factor = 1.2
			planet = {
				exists = sector
				sector = {
					is_same_value = parameter:sector
				}
				distance = {
					source = parameter:owner_empire.capital_scope
					min_distance = 100 #150
				}
			}
		}

		# +20% attraction if faction leader is ruler
		modifier = {
			factor = 1.2
			exists = owner
			owner = {
				exists = ruler
				ruler = {
					exists = pop_faction
					pop_faction = {
						is_same_value = from
					}
				}
			}
		}		
		
		modifier = {
			factor = 0.5
			has_pop_flag = "political_apathy"
			NOT = { has_modifier = moved_by_protest }
		}
		
		modifier = { #This comes AFTER all the other checks so that the factors don't multiply the 100
			add = 100
			has_modifier = "pop_rebellious"
		}
		
		modifier = {
			factor = 0
			planet = {
				sector_controlled = no
			}
		}
		
		modifier = {
			factor = 0
			planet = {
				NOT = {
					AND = {
						exists = sector
						sector = {
							is_same_value = parameter:sector
						}
					}
				}
			}
		}

		# set to 0 if over 35% happiness (NOT ANYMORE!)
#		modifier = {
#			factor = 0
#			happiness > 0.35
#		}

		# set to 0 if robotic
		modifier = {
			factor = 0
			is_slavebot = yes
		}
	}
	
	on_create = {
		set_name = random
	}

	flag = {
		icon = {
			category = "blocky"
			file = "flag_blocky_22.dds"
		}
		background= {
			category = "backgrounds"
			file = "diagonal.dds"
		}
		colors={
			"dark_teal"
			"teal"
			"null"
			"null"
		}
	}
	
	#mandate = {
	#	key = "mandate_planet_separatist"
	#	parameter_transfer = {
	#		sector = sector
	#	}
	#	weight = { 
	#		base = 100
	#	}
	#}
	
	on_set_leader = {
		generate_leader = yes
	}
	
	demand = {
		title = "POP_FACTION_SECTOR_SEPARATIST_DEMAND_TITLE"
		desc = "POP_FACTION_SECTOR_SEPARATIST_DEMAND_DESC"
	}
	
	actions = {		
		grant_independence = {
			title = "POP_FACTION_SECTOR_SEPARATIST_GRANT_INDEPENDENCE_TITLE"
			description = "POP_FACTION_SECTOR_SEPARATIST_GRANT_INDEPENDENCE_DESC"
			
			valid = {
				custom_tooltip = {
					text = "POP_FACTION_SECTOR_SEPARATIST_IS_NOT_OCCUPIED"
					parameter:sector = { debug_scope_type = sector
						NOT = {
							any_planet = {
								is_occupied_flag = yes
							}
						}
					}
				}
				
				custom_tooltip = {
					text = "POP_FACTION_SECTOR_SEPARATIST_NO_GROUND_COMBAT"
					parameter:sector = { debug_scope_type = sector
						NOT = {
							any_planet = {
								has_ground_combat = yes
							}
						}
					}
				}
			}
			
			effect = {
				hidden_effect = {
					if = {
						limit = {
							has_pop_faction_flag = mandated_release
						}
						parameter:sector = { debug_scope_type = sector
							capital_scope = { save_event_target_as = released_planet }
						}
						owner = { country_event = { id = mandate.100 } }
					}
					root.owner = {
						random_owned_planet = {
							limit = {
								exists = sector
								sector = { is_same_value = parameter:sector }
								any_pop = {
									NOT = {
										is_sentient = no
										pop_has_trait = trait_robotic_1
										pop_has_trait = trait_robotic_2
									}
								}
							}
							random_owned_pop = {
								limit = {
									NOT = {
										is_sentient = no
										pop_has_trait = trait_robotic_1
										pop_has_trait = trait_robotic_2
									}
								}
								species = { save_event_target_as = spawn_species }
							}
						}
						create_country = {
							type = default
							government = gov_provisional
							species = event_target:spawn_species
							flag = random
							ethos = random
							ignore_initial_colony_error = yes
							effect = {
								root.owner = {
									establish_communications_no_message = prev
								}
								parameter:sector = { debug_scope_type = sector
									every_planet = {
										limit = { 
											exists = owner
											owner = {
												is_same_value = root.owner 
											}
										}
										set_controller = prevprev
										set_owner = prevprev
									}
								}
								set_name = random
								set_subject_of = {
									who = parameter:owner_empire
									subject_type = vassal
								}
							}
						}
					}
					kill_pop_faction = yes
				}
			}
			
			ai_weight = {
				base = 0
			}
		}
	}
	
	support_effects = {
		smuggler_influence = {
			title = SMUGGLER_INFLUENCE
			min_support = 0.0
			max_support = 0.9
			
			spawn_chance = {
				base = 0
				modifier = {
					add = 1
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 1
							}
						}
					}
				}
				modifier = {
					add = 2
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 2
							}
						}
					}
				}
				modifier = {
					add = 3
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 3
							}
						}
					}
				}
				modifier = {
					add = 4
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 4
							}
						}
					}
				}
				modifier = {
					add = 5
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 5
							}
						}
					}
				}
				modifier = {
					add = 6
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 6
							}
						}
					}
				}
				modifier = {
					add = 7
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 7
							}
						}
					}
				}
				modifier = {
					add = 8
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 8
							}
						}
					}
				}
				modifier = {
					add = 9
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value = 9
							}
						}
					}
				}
				modifier = {
					add = 10
					custom_tooltip = {
						text = "FACTION_SMUGGLER_INFLUENCE_TOOLTIP"
						root.owner = {
							check_variable = {
								which = var_smuggler_influence
								value > 9
							}
						}
					}
				}
			}
			
			effect = {
				add_support = 0.1
				custom_tooltip = "SMUGGLER_INFLUENCE_EFFECT"
				hidden_effect = {
					root.owner = {
						set_variable = {
							which = var_smuggler_influence
							value = 0
						}
					}
				}
			}
		}
		
		
		stage_protest = {
			title = STAGE_PROTEST
			min_support = 0.25
			max_support = 0.85
			
			spawn_chance = {
				base = 5
				modifier = {
					add = 5
					support > 0.45
				}
				modifier = {
					factor = 0.2
					parameter:owner_empire = {
						has_policy_flag = protesting_illegal
					}
				}
				modifier = {
					add = 1
					parameter:owner_empire = {
						has_policy_flag = protesting_illegal
						has_policy_flag = authority_02
					}
				}
				modifier = {
					add = 2
					parameter:owner_empire = {
						has_policy_flag = protesting_illegal
						has_policy_flag = authority_01
					}
				}
				modifier = {
					factor = 0
					parameter:owner_empire = {
						has_modifier = killed_protesters
					}
				}
			}
			
			effect = {
				custom_tooltip = "STAGE_PROTEST_EFFECT"
				hidden_effect = {
					pop_faction_event = {
						id = bo_faction.202
					}
				}
			}
		}
		
		civil_disobedience = {
			title = CIVIL_DISOBEDIENCE
			min_support = 0.55
			max_support = 1.0
			
			spawn_chance = {
				base = 100
				modifier = {
					factor = 0
					parameter:owner_empire = {
						has_edict = martial_law
					}
				}
			}
			
			effect = {
				custom_tooltip = CIVIL_DISOBEDIENCE_EFFECT
				hidden_effect = {
					every_owned_pop = {
						add_modifier = {
							modifier = pop_on_strike
							days = 30
						}
					}
				}
			}
		}
		
		# Rebellion
		rebellion = {
			title = "POP_FACTION_PLANET_SEPARATIST_REBELLION_TITLE"
			min_support = 1.0
			max_support = 1.0
			
			spawn_chance = { 
				value = 25
				
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POP_FACTION_PLANET_SEPARATIST_LEADER_IS_RULER"
						owner = {
							exists = ruler
							ruler = {
								is_same_value = root.leader
							}
						}
					}
				}
				
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POP_FACTION_PLANET_SEPARATIST_OCCUPIED"
						parameter:sector = { debug_scope_type = sector
							any_planet = {
								is_colony = yes
								is_occupied_flag = yes
							}
						}
					}
				}
				
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "POP_FACTION_PLANET_SEPARATIST_COMBAT"
						parameter:sector = { debug_scope_type = sector
							any_planet = {
								is_colony = yes
								has_ground_combat = yes
							}
						}
					}
				}
				
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "CURRENTLY_IN_CIVIL_WAR"
						any_country = {
							AND = {
								has_relation_flag = {
									who = parameter:owner_empire
									flag = revolutionary
								}
								is_at_war = no
							}
						}
					}
				}
				
				modifier = {
					factor = 0
					custom_tooltip = {
						text = "REBELS_REVOLT_AT_WAR"
						parameter:owner_empire = { is_at_war = yes }
					}
				}
			}
			
			effect = {
				custom_tooltip = "REVOLUTIONARIES_REVOLT_EFFECT"
				hidden_effect = {
					root.owner = {
						random_owned_planet = {
							limit = {
								exists = sector
								sector = { is_same_value = parameter:sector }
								any_pop = {
									NOT = {
										pop_has_trait = trait_robotic_1
										pop_has_trait = trait_robotic_2
										#pop_has_trait = trait_robotic_3
									}
								}
							}
							save_event_target_as = planet
						}
						event_target:planet = {
							random_owned_pop = {
								limit = {
									NOT = {
										is_sentient = no
										pop_has_trait = trait_robotic_1
										pop_has_trait = trait_robotic_2
									}
								}
								species = { save_event_target_as = spawn_species }
							}
						}
					}
					owner = {
						#owner_species = { save_event_target_as = spawn_species }
						switch = {
							trigger = has_ethic

							ethic_fanatic_xenophile = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_xenophobe"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_xenophobe = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_xenophile"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_pacifist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_militarist"
											ethic = "ethic_materialist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_militarist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_pacifist"
											ethic = "ethic_spiritualist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_materialist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_spiritualist"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_spiritualist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_materialist"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_individualist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_collectivist"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_fanatic_collectivist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = "ethic_fanatic_individualist"
											ethic = "ethic_militarist"
										}
										flag = random
									}
								}
							}

							ethic_collectivist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_individualist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_materialist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_spiritualist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_xenophile = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_xenophobe = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_pacifist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}

							ethic_militarist = {
								every_planet = {
									limit = {
										exists = sector
										sector = { is_same_value = parameter:sector }
									}
									create_rebels = {
										name = random
										government = gov_provisional
										species = event_target:spawn_species
										ethos = {
											ethic = random
											ethic = random
										}
										flag = random
									}
								}
							}
						}
						
						save_event_target_as = overlord
					}
					
					last_created_country = {
						set_relation_flag = {
							who = parameter:owner_empire
							flag = revolutionary
						}
						set_subject_of = {
							who = parameter:owner_empire
							subject_type = vassal
						}
						add_war_demand = {
							type = "independence"
							parameter:country = event_target:overlord
							warscore_cost = 50
							target = this
						}
						save_event_target_as = rebel_side
						set_variable = {
							which = rebel_fleets_to_be_created
							value = 0
						}
						set_variable = {
							which = rebel_legitimacy
							value = 0
						}
					}
					
					every_planet = {
						limit = {
							exists = sector
							sector = { is_same_value = parameter:sector }
						}
						last_created_country = {
							add_war_demand = {
								type = "rebel_liberate_planet"
								parameter:planet = prev
								warscore_cost = 10
								target = this
							}
						}
					}
					
					every_owned_pop = {
						event_target:rebel_side = {
							change_variable = {
								which = rebel_fleets_to_be_created
								value = 1
							}
						}
						remove_modifier = "pop_rebellious"
						add_modifier = {
							modifier = "recently_revolted"
							days = 5475
						}
						planet = {
							create_army = {
								owner = last_created
								species = prev
								type = "revolutionary_army"
							}
						}
					}
					#kill_pop_faction = yes
					pop_faction_event = { id = faction.100 }
				}
			}
		}
	}
}