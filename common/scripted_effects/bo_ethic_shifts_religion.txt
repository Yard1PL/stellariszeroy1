#These effects let you nudge pop ethics. This is used by political parties when they influence pops.

#spiritualist
make_pop_more_spiritualist = {
	if = {
		limit = {
			pop_has_ethic = ethic_spiritualist
		}
		pop_add_ethic = ethic_fanatic_spiritualist
	}
	if = {
		limit = {
			NOT = {
				OR = {
					pop_has_ethic = ethic_fanatic_materialist
					pop_has_ethic = ethic_materialist
					pop_has_ethic = ethic_spiritualist
					pop_has_ethic = ethic_fanatic_spiritualist
				}
			}
		}
		pop_add_ethic = ethic_spiritualist
	}
	if = {
		limit = {
			pop_has_ethic = ethic_materialist
		}
		pop_remove_ethic = ethic_materialist
	}
	if = {
		limit = {
			pop_has_ethic = ethic_fanatic_materialist
		}
		pop_add_ethic = ethic_materialist
	}
}

make_country_more_spiritualist = {
	if = {
		limit = {
			has_ethic = ethic_spiritualist
		}
		country_add_ethic = ethic_fanatic_spiritualist
	}
	if = {
		limit = {
			NOT = {
				OR = {
					has_ethic = ethic_fanatic_materialist
					has_ethic = ethic_materialist
					has_ethic = ethic_spiritualist
					has_ethic = ethic_fanatic_spiritualist
				}
			}
		}
		country_add_ethic = ethic_spiritualist
	}
	if = {
		limit = {
			has_ethic = ethic_materialist
		}
		country_remove_ethic = ethic_materialist
	}
	if = {
		limit = {
			has_ethic = ethic_fanatic_materialist
		}
		country_add_ethic = ethic_materialist
	}
}

#materialist
make_pop_more_materialist = {
	if = {
		limit = {
			pop_has_ethic = ethic_materialist
		}
		pop_add_ethic = ethic_fanatic_materialist
	}
	if = {
		limit = {
			NOT = {
				OR = {
					pop_has_ethic = ethic_fanatic_spiritualist
					pop_has_ethic = ethic_spiritualist
					pop_has_ethic = ethic_materialist
					pop_has_ethic = ethic_fanatic_materialist
				}
			}
		}
		pop_add_ethic = ethic_materialist
	}
	if = {
		limit = {
			pop_has_ethic = ethic_spiritualist
		}
		pop_remove_ethic = ethic_spiritualist
	}
	if = {
		limit = {
			pop_has_ethic = ethic_fanatic_spiritualist
		}
		pop_add_ethic = ethic_spiritualist
	}
}

make_country_more_materialist = {
	if = {
		limit = {
			has_ethic = ethic_materialist
		}
		country_add_ethic = ethic_fanatic_materialist
	}
	if = {
		limit = {
			NOT = {
				OR = {
					has_ethic = ethic_fanatic_spiritualist
					has_ethic = ethic_spiritualist
					has_ethic = ethic_materialist
					has_ethic = ethic_fanatic_materialist
				}
			}
		}
		country_add_ethic = ethic_materialist
	}
	if = {
		limit = {
			has_ethic = ethic_spiritualist
		}
		country_remove_ethic = ethic_spiritualist
	}
	if = {
		limit = {
			has_ethic = ethic_fanatic_spiritualist
		}
		country_add_ethic = ethic_spiritualist
	}
}