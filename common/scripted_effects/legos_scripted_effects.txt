#	Example:
# 
#	example_effect = {
#		add_energy = -100
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_effect = yes
#	}
#

#	Example:
# 
#	example_effect = {
#		add_energy = -100
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_effect = yes
#	}
#

#Call from a country scope
energy_to_variable = {
	set_variable = { which = energy_to_variable value = 0 }
	set_variable = { which = energy_to_variable_temp value = 0 }
	while = {
		limit = {
			energy > 9999
		}
		change_variable = { which = energy_to_variable_temp value = 10000 }
		add_energy = -10000
	}
	while = {
		limit = {
			energy > 999
		}
		change_variable = { which = energy_to_variable_temp value = 1000 }
		add_energy = -1000
	}
	while = {
		limit = {
			energy > 99
		}
		change_variable = { which = energy_to_variable_temp value = 100 }
		add_energy = -100
	}
	while = {
		limit = {
			energy > 9
		}
		change_variable = { which = energy_to_variable_temp value = 10 }
		add_energy = -10
	}
	while = {
		limit = {
			energy > 0
		}
		change_variable = { which = energy_to_variable_temp value = 1 }
		add_energy = -1
	}
	if = { 
		limit = { energy < 0 } 
		while = {
			limit = {
				energy < -9999
			}
			change_variable = { which = energy_to_variable_temp value = -10000 }
			add_energy = 10000
		}
		while = {
			limit = {
				energy < -999
			}
			change_variable = { which = energy_to_variable_temp value = -1000 }
			add_energy = 1000
		}
		while = {
			limit = {
				energy < -99
			}
			change_variable = { which = energy_to_variable_temp value = -100 }
			add_energy = 100
		}
		while = {
			limit = {
				energy < -9
			}
			change_variable = { which = energy_to_variable_temp value = -10 }
			add_energy = 10
		}
		while = {
			limit = {
				energy < 0
			}
			change_variable = { which = energy_to_variable_temp value = -1 }
			add_energy = 1
		}		
	}
	set_variable = { which = energy_to_variable value = energy_to_variable_temp }
	while = {
		limit = {
			check_variable = { which = energy_to_variable_temp value > 9999 }
		}
		change_variable = { which = energy_to_variable_temp value = -10000 }
		add_energy = 10000
	}
	while = {
		limit = {
			check_variable = { which = energy_to_variable_temp value > 999 }
		}
		change_variable = { which = energy_to_variable_temp value = -1000 }
		add_energy = 1000
	}
	while = {
		limit = {
			check_variable = { which = energy_to_variable_temp value > 99 }
		}
		change_variable = { which = energy_to_variable_temp value = -100 }
		add_energy = 100
	}
	while = {
		limit = {
			check_variable = { which = energy_to_variable_temp value > 9 }
		}
		change_variable = { which = energy_to_variable_temp value = -10 }
		add_energy = 10
	}
	while = {
		limit = {
			check_variable = { which = energy_to_variable_temp value > 0 }
		}
		change_variable = { which = energy_to_variable_temp value = -1 }
		add_energy = 1
	}
	if = { 
		limit = { check_variable = { which = energy_to_variable_temp value < 0 } } 
		while = {
			limit = {
				check_variable = { which = energy_to_variable_temp value < -9999 }
			}
			change_variable = { which = energy_to_variable_temp value = 10000 }
			add_energy = -10000
		}
		while = {
			limit = {
				check_variable = { which = energy_to_variable_temp value < -999 }
			}
			change_variable = { which = energy_to_variable_temp value = 1000 }
			add_energy = -1000
		}
		while = {
			limit = {
				check_variable = { which = energy_to_variable_temp value < -99 }
			}
			change_variable = { which = energy_to_variable_temp value = 100 }
			add_energy = -100
		}
		while = {
			limit = {
				check_variable = { which = energy_to_variable_temp value < -9 }
			}
			change_variable = { which = energy_to_variable_temp value = 10 }
			add_energy = -10
		}
		while = {
			limit = {
				check_variable = { which = energy_to_variable_temp value < 0 }
			}
			change_variable = { which = energy_to_variable_temp value = 1 }
			add_energy = -1
		}		
	}	
}

#Call from a country scope
minerals_to_variable = {
	set_variable = { which = minerals_to_variable value = 0 }
	set_variable = { which = minerals_to_variable_temp value = 0 }
	while = {
		limit = {
			minerals > 9999
		}
		change_variable = { which = minerals_to_variable_temp value = 10000 }
		add_minerals = -10000
	}
	while = {
		limit = {
			minerals > 999
		}
		change_variable = { which = minerals_to_variable_temp value = 1000 }
		add_minerals = -1000
	}
	while = {
		limit = {
			minerals > 99
		}
		change_variable = { which = minerals_to_variable_temp value = 100 }
		add_minerals = -100
	}
	while = {
		limit = {
			minerals > 9
		}
		change_variable = { which = minerals_to_variable_temp value = 10 }
		add_minerals = -10
	}
	while = {
		limit = {
			minerals > 0
		}
		change_variable = { which = minerals_to_variable_temp value = 1 }
		add_minerals = -1
	}
	if = { 
		limit = { minerals < 0 } 
		while = {
			limit = {
				minerals < -9999
			}
			change_variable = { which = minerals_to_variable_temp value = -10000 }
			add_minerals = 10000
		}
		while = {
			limit = {
				minerals < -999
			}
			change_variable = { which = minerals_to_variable_temp value = -1000 }
			add_minerals = 1000
		}
		while = {
			limit = {
				minerals < -99
			}
			change_variable = { which = minerals_to_variable_temp value = -100 }
			add_minerals = 100
		}
		while = {
			limit = {
				minerals < -9
			}
			change_variable = { which = minerals_to_variable_temp value = -10 }
			add_minerals = 10
		}
		while = {
			limit = {
				minerals < 0
			}
			change_variable = { which = minerals_to_variable_temp value = -1 }
			add_minerals = 1
		}		
	}
	set_variable = { which = minerals_to_variable value = minerals_to_variable_temp }
	while = {
		limit = {
			check_variable = { which = minerals_to_variable_temp value > 9999 }
		}
		change_variable = { which = minerals_to_variable_temp value = -10000 }
		add_minerals = 10000
	}
	while = {
		limit = {
			check_variable = { which = minerals_to_variable_temp value > 999 }
		}
		change_variable = { which = minerals_to_variable_temp value = -1000 }
		add_minerals = 1000
	}
	while = {
		limit = {
			check_variable = { which = minerals_to_variable_temp value > 99 }
		}
		change_variable = { which = minerals_to_variable_temp value = -100 }
		add_minerals = 100
	}
	while = {
		limit = {
			check_variable = { which = minerals_to_variable_temp value > 9 }
		}
		change_variable = { which = minerals_to_variable_temp value = -10 }
		add_minerals = 10
	}
	while = {
		limit = {
			check_variable = { which = minerals_to_variable_temp value > 0 }
		}
		change_variable = { which = minerals_to_variable_temp value = -1 }
		add_minerals = 1
	}
	if = { 
		limit = { check_variable = { which = minerals_to_variable_temp value < 0 } } 
		while = {
			limit = {
				check_variable = { which = minerals_to_variable_temp value < -9999 }
			}
			change_variable = { which = minerals_to_variable_temp value = 10000 }
			add_minerals = -10000
		}
		while = {
			limit = {
				check_variable = { which = minerals_to_variable_temp value < -999 }
			}
			change_variable = { which = minerals_to_variable_temp value = 1000 }
			add_minerals = -1000
		}
		while = {
			limit = {
				check_variable = { which = minerals_to_variable_temp value < -99 }
			}
			change_variable = { which = minerals_to_variable_temp value = 100 }
			add_minerals = -100
		}
		while = {
			limit = {
				check_variable = { which = minerals_to_variable_temp value < -9 }
			}
			change_variable = { which = minerals_to_variable_temp value = 10 }
			add_minerals = -10
		}
		while = {
			limit = {
				check_variable = { which = minerals_to_variable_temp value < 0 }
			}
			change_variable = { which = minerals_to_variable_temp value = 1 }
			add_minerals = -1
		}		
	}	
}