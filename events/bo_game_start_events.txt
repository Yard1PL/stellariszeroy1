namespace = bo_game_start

event = { #Assign pops apathy at the beginning
	id = bo_game_start.1
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		every_country = {
			every_owned_pop = {
				random_list = {
					80 = {
						set_pop_flag = political_apathy
					}
					20 = {
						#Do Nothing
					}
				}
			}
		}
	}
}

event = { 
	id = bo_game_start.2
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		every_country = {
			set_timed_country_flag = { flag = game_start days = 1 }
			if = {
				limit = {
					has_government = gov_democracy
				}
				initialize_gov_democracy = yes
			}			
			if = {
				limit = {
					has_government = gov_corrupt_democracy
				}
				initialize_gov_corrupt_democracy = yes
			}
			if = {
				limit = {
					has_government = gov_imperial
				}
				initialize_gov_imperial = yes
			}
			if = {
				limit = {
					has_government = gov_mega_corporation
				}
				initialize_gov_mega_corporation = yes
			}
			if = {
				limit = {
					has_government = gov_pirate_enclave
				}
				initialize_gov_pirate_enclave = yes
			}
		}
	}
}

event = {
	id = bo_game_start.3
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		every_country = {
			limit = { 
				OR = {
					is_country_type = default 
					is_country_type = fallen_empire 
				}
			}
			every_owned_planet = {
				limit = { is_homeworld = yes NOT = { has_modifier = homeworld }  }
				add_modifier = {
					modifier = homeworld
					days = -1
				}
			}
		}	
	}
}