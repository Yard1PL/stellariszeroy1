namespace = bo_civil_war

country_event = { #Rebels not annexed
	id = bo_civil_war.1
	title = bo_civil_war.1.title
	desc = bo_civil_war.1.desc
	picture = GFX_evt_outpost
	
	trigger = {
		any_country = {
			AND = {
				has_relation_flag = {
					who = root
					flag = revolutionary
				}
				is_subject_type = revolution_subject
				is_at_war = no
				num_owned_planets = 1
			}
		}
	}
	
#	mean_time_to_happen = {
#		months = 6
#	}

	immediate = {
		every_country = {
			limit = {
				AND = {
					has_relation_flag = {
						who = root
						flag = revolutionary
					}
					is_subject_type = revolution_subject
					is_at_war = no
					num_owned_planets = 1
				}
			}
			every_owned_planet = {
				set_owner = root
			}
		}
	}
	
	option = {
		name = UNFORTUNATE
		add_modifier = {
			modifier = rebel_white_peace
			days = 7200
		}
	}
}

country_event = { #Spawn rebels ships and give the rebels the energy to maintain them
	id = bo_civil_war.2
	
	hide_window = yes
	
	trigger = {
		num_owned_planets = 1
		any_country = {
			reverse_has_relation_flag = {
				who = root
				flag = revolutionary
			}
		}
		NOT = { has_country_flag = rebels_initialized }
	}
	
	immediate = {
		set_country_flag = rebels_initialized
		add_energy = 2500
		add_minerals = 1000
		random_owned_planet = {
			save_event_target_as = rebel_base
		}
		multiply_variable = {
			which = rebel_fleets_to_be_created
			value = 0.4
		}
		while = {
			limit = {
				check_variable = {
					which = rebel_fleets_to_be_created
					value > 0
				}
			}
			change_variable = {
				which = rebel_fleets_to_be_created
				value = -1
			}
			random_list = {
				1 = {
					create_fleet = {
						name = "Rebel Fleet"
						effect = {
							set_owner = root
							spawn_rebel_ships = yes
							set_location = {
								target = event_target:rebel_base
								distance = 35
								angle = random
							}
						}
					}
				}
			}
		}
		
		#Foreign Events
		random_country = {
			limit = {
				reverse_has_relation_flag = {
					who = root
					flag = revolutionary
				}
			}
			save_event_target_as = loyalists
		}
		if = { #Only if NOT a slave revolt
			limit = {
				NOT = { has_country_flag = slave_revolt }
			}
			every_country = {
				limit = {
					has_established_contact = event_target:loyalists
					has_established_contact = root
					is_country_type = default
					NOT = { is_same_empire = root }
					NOT = { is_same_empire = event_target:loyalists }
					NOT = { is_in_federation_with = event_target:loyalists }
					NOT = { 
						any_subject = {
							is_same_value = event_target:loyalists
						}
					}
					NOT = {
						event_target:loyalists = {
							any_subject = {
								is_same_value = prevprev
							}
						}
					}
					NOT = { has_defensive_pact = event_target:loyalists }
					NOT = { is_at_war_with = event_target:loyalists }
					is_subject = no
					OR = {
						has_federation = no
						is_federation_leader = yes
					}
				}
				country_event = {
					id = bo_civil_war.3
				}
			}
		}
		every_country = { #Ally Notification
			limit = {
				NOT = { is_same_value = event_target:loyalists }
				OR = {
					has_defensive_pact = event_target:loyalists
					is_in_federation_with = event_target:loyalists
					any_subject = {
						is_same_value = event_target:loyalists
					}
					event_target:loyalists = {
						any_subject = {
							is_same_value = prevprev
						}
					}
				}
			}
			country_event = {
				id = bo_notification.13
			}
			root = {
				subtract_variable = {
					which = rebel_legitimacy
					value = 1
				}
			}
		}
	}
}

country_event = { #Rebels ask for support
	id = bo_civil_war.3
	title = bo_civil_war.3.title
	desc = bo_civil_war.3.desc
	picture_event_data = {
		portrait = from
		planet_background = from.capital
		graphical_culture = from.capital
		room = from
	}
	
	diplomatic = yes
	is_triggered_only = yes
	
	
	option = { #JOIN LOYALISTS
		name = bo_civil_war.3.join_loyalists
		hidden_effect = { #Copy changes to the defensive pact/federation effect above
			join_war = event_target:loyalists
			from = {
				subtract_variable = {
					which = rebel_legitimacy
					value = 1
				}
			}
			every_subject = {
				limit = {
					OR = {
						is_subject_type = vassal
						is_subject_type = legal_entity
					}
				}
				join_war = event_target:loyalists
			}
			every_country = {
				limit = {
					is_in_federation_with = root
					NOT = { is_same_value = root }
				}
#				join_war = event_target:loyalists
				from = { save_event_target_as = side_we_are_fighting }
				event_target:loyalists = { save_event_target_as = side_we_are_helping }
				event_target:loyalists = { save_event_target_as = loyalist_side }
				country_event = {
					id = bo_civil_war.4
				}
			}
			event_target:loyalists = {
				country_event = {
					id = bo_notification.10
				}
			}
		}
		AI_chance = {
			base = 0
			
			modifier = {
				add = 12
				is_protective_to = event_target:loyalists
			}
			
			modifier = {
				add = 10
				is_friendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 2
				is_cordial_to = event_target:loyalists
			}
			
			modifier = {
				add = 10
				has_association_status = event_target:loyalists
			}
		}
	}
	
	option = { #SUPPORT LOYALISTS
		name = bo_civil_war.3.recognize_loyalists
		hidden_effect = {
			from = {
				add_opinion_modifier = {
					modifier = opinion_declared_illegitimate
					who = root
				}
			}
			event_target:loyalists = {
				add_opinion_modifier = {
					modifier = opinion_declared_legitimate
					who = root
				}
			}
			from = {
				subtract_variable = {
					which = rebel_legitimacy
					value = 1
				}
			}
		}
		AI_chance = {
			base = 0
			
			modifier = {
				add = 6
				is_protective_to = event_target:loyalists
			}
			
			modifier = {
				add = 5
				is_friendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 10
				is_cordial_to = event_target:loyalists
			}
		}
	}
	
	option = { #REMAIN NEUTRAL
		name = REMAIN_NEUTRAL
		custom_tooltip = DESPICABLE_NEUTRAL_1
		AI_chance = {
			base = 5
			modifier = {
				add = 10
				is_neutral_to event_target:loyalists
			}
		}
	}
	
	option = { #SUPPORT REBELS
		name = bo_civil_war.3.recognize_rebels
		hidden_effect = {
			from = {
				add_opinion_modifier = {
					modifier = opinion_declared_legitimate
					who = root
				}
			}
			event_target:loyalists = {
				add_opinion_modifier = {
					modifier = opinion_declared_illegitimate
					who = root
				}
			}
			from = {
				change_variable = {
					which = rebel_legitimacy
					value = 1
				}
			}
		}
		AI_chance = {
			base = 0
			modifier = {
				add = 6
				is_rival = event_target:loyalists
			}
			
			modifier = {
				add = 10
				is_unfriendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 5
				is_hostile_to = event_target:loyalists
			}
			
			modifier = {
				add = 6
				is_threatened_to = event_target:loyalists
			}
		}
	}
	
	option = { #JOIN REBELS
		name = bo_civil_war.3.join_rebels
		hidden_effect = {
			join_war = from
			from = {
				change_variable = {
					which = rebel_legitimacy
					value = 1
				}
			}
			every_subject = {
				limit = {
					OR = {
						is_subject_type = vassal
						is_subject_type = legal_entity
					}
				}
				join_war = from
			}
			every_country = {
				limit = {
					is_in_federation_with = root
					NOT = { is_same_value = root }
				}
#				join_war = from
				event_target:loyalists = { save_event_target_as = loyalist_side }
				event_target:loyalists = { save_event_target_as = side_we_are_fighting }
				from = { save_event_target_as = side_we_are_helping }
				country_event = {
					id = bo_civil_war.4
				}
			}
			event_target:loyalists = {
				country_event = {
					id = bo_notification.11
				}
			}
		}
		trigger = {
			NOT = { has_non_aggression_pact = event_target:loyalists }
			NOT = { has_defensive_pact = event_target:loyalists }
			NOT = { has_association_status = event_target:loyalists }
		}
		AI_chance = {
			base = 0

			modifier = {
				add = 10
				is_rival = event_target:loyalists
			}
			
			modifier = {
				add = 6
				is_unfriendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 12
				is_hostile_to = event_target:loyalists
			}

			modifier = {
				factor = 1.5
				has_ai_personality_behaviour = liberator
			}
			
			modifier = {
				factor = 1.3
				has_ai_personality_behaviour = opportunist 
			}
			
			modifier = {
				factor = 0
				is_at_war = yes
			}
			modifier = {
				factor = 0
				OR = {
					has_non_aggression_pact = event_target:loyalists
					has_defensive_pact = event_target:loyalists
					has_association_status = event_target:loyalists
				}
			}
		}
	}
}

country_event = {
	id = bo_civil_war.4
	title = bo_civil_war.4.title
	desc = bo_civil_war.4.desc
	picture_event_data = {
		portrait = from
		planet_background = from.capital
		graphical_culture = from.capital
		room = from
	}
	
	diplomatic = yes
	is_triggered_only = yes
	
	option = {
		name = REMAIN_NEUTRAL
		custom_tooltip = DESPICABLE_NEUTRAL_1
		AI_chance = {
			base = 5
			modifier = {
				add = 10
				is_neutral_to = event_target:loyalist_side
			}
		}
	}
	
	option = {
		name = bo_civil_war.4.support_federation
		trigger = {
			OR = {
				event_target:loyalist_side = {
					NOT = { is_same_value = event_target:side_we_are_fighting }
				}
				AND = {
					NOT = { has_non_aggression_pact = event_target:loyalists }
					NOT = { has_defensive_pact = event_target:loyalists }
					NOT = { has_association_status = event_target:loyalists }
				}
			}
		}
		hidden_effect = {
			join_war = event_target:side_we_are_helping
			every_subject = {
				limit = {
					OR = {
						is_subject_type = vassal
						is_subject_type = legal_entity
					}
				}
				join_war = event_target:side_we_are_helping
			}
			if = {
				limit = {
					event_target:loyalists = { is_same_value = event_target:side_we_are_helping }
				}
				event_target:loyalists = {
					country_event = {
						id = bo_notification.10
					}
				}
			}
			if = {
				limit = {
					NOT = { event_target:loyalists = { is_same_value = event_target:side_we_are_helping } }
				}
				event_target:loyalists = {
					country_event = {
						id = bo_notification.11
					}
				}
			}
		}
		
		AI_chance = {
			base = 2
			#If they are attacking the loyalists
			modifier = {
				add = 10
				event_target:loyalist_side = {
					is_same_value = event_target:side_we_are_fighting
				}
				is_rival = event_target:loyalist_side
			}
			
			modifier = {
				add = 6
				event_target:loyalist_side = {
					is_same_value = event_target:side_we_are_fighting
				}
				is_unfriendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 12
				event_target:loyalist_side = {
					is_same_value = event_target:side_we_are_fighting
				}
				is_hostile_to = event_target:loyalists
			}

			modifier = {
				factor = 1.5
				event_target:loyalist_side = {
					is_same_value = event_target:side_we_are_fighting
				}
				has_ai_personality_behaviour = liberator
			}
			
			modifier = {
				factor = 1.3
				event_target:loyalist_side = {
					is_same_value = event_target:side_we_are_fighting
				}
				has_ai_personality_behaviour = opportunist 
			}
			
			#If they are attacking the rebels
			modifier = {
				add = 12
				event_target:loyalist_side = {
					NOT = { is_same_value = event_target:side_we_are_fighting }
				}
				is_protective_to = event_target:loyalists
			}
			
			modifier = {
				add = 10
				event_target:loyalist_side = {
					NOT = { is_same_value = event_target:side_we_are_fighting }
				}
				is_friendly_to = event_target:loyalists
			}
			
			modifier = {
				add = 2
				event_target:loyalist_side = {
					NOT = { is_same_value = event_target:side_we_are_fighting }
				}
				is_cordial_to = event_target:loyalists
			}
			
			modifier = {
				add = 10
				event_target:loyalist_side = {
					NOT = { is_same_value = event_target:side_we_are_fighting }
				}
				has_association_status = event_target:loyalists
			}
		}
	}
}

country_event = { #Remove revolutionary flag and set the new nations authority.
	id = bo_civil_war.5
	hide_window = yes
	
	is_triggered_only = yes #Triggered in the war demand effect
	
	immediate = {
		if = {
			limit = {
				check_variable = {
					which = rebel_legitimacy
					value > 0
				}
			}
			set_policy = {
				policy = authority
				option = authority_03
				cooldown = yes
			}
			every_country = {
				limit = {
					has_established_contact = root
				}
				country_event = {
					id = bo_notification.3
				}
			}
		}
		if = {
			limit = {
				check_variable = {
					which = rebel_legitimacy
					value < 1
				}
			}
			random_list = { #Extreme ends of authority should be the least stable
				50 = {
					set_policy = {
						policy = authority
						option = authority_01
						cooldown = yes
					}
				}
				50 = {
					set_policy = {
						policy = authority
						option = authority_05
						cooldown = yes
					}
				}
			every_country = {
				limit = {
					has_established_contact = root
				}
				country_event = {
					id = bo_notification.4
				}
			}
		}
		every_owned_pop = {
			remove_modifier = pop_heretic
			remove_modifier = pop_upset_with_reform
			remove_modifier = pop_very_upset_with_reform
			remove_modifier = pop_rebellious
		}
		remove_modifier = sponsored_rebels
	}
}