namespace = bo_empire

country_event = { #Empire succession
	id = bo_empire.1
	title = bo_empire.1.title
	desc = bo_empire.1.desc
	picture = GFX_evt_galactic_senate
	
	is_triggered_only = yes
	
	trigger = {
		has_government = gov_imperial
		years_passed > 0
	}
	
	immediate = {
		country_event = {
			id = bo_empire.2
			days = 1800
		}
	}
	
	option = {
		name = CURSES
		hidden_effect = {
			remove_modifier = recent_succession
		}
		add_modifier = {
			modifier = recent_succession
			days = 720
		}
	}
}

country_event = { #Select new empire ethics
	id = bo_empire.2
	title = bo_empire.2.title
	desc = bo_empire.2.desc
	picture = GFX_evt_throne_room
	
	is_triggered_only = yes
	
	trigger = {
		has_government = gov_imperial
	}
	
	option = {
		name = bo_empire.2.status_quo
		ai_chance = {
			base = 6
		}
	}
	
	option = {
		name = bo_empire.2.individualist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_individualist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_INDIVIDUALIST_ACCEPT_TOOLTIP
		make_country_more_individualist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_individualist #Adjust Per Option
							has_ethic = ethic_fanatic_individualist #Adjust Per Option
							has_ethic = ethic_collectivist #Adjust Per Option
							has_ethic = ethic_fanatic_collectivist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_collectivist #Adjust Per Option
						has_ethic = ethic_fanatic_collectivist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_individualist #Adjust Per Option
						has_ethic = ethic_fanatic_individualist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Collectivist Reform
		name = bo_empire.2.collectivist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_collectivist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_COLLECTIVIST_ACCEPT_TOOLTIP
		make_country_more_collectivist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_individualist #Adjust Per Option
							has_ethic = ethic_fanatic_individualist #Adjust Per Option
							has_ethic = ethic_collectivist #Adjust Per Option
							has_ethic = ethic_fanatic_collectivist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_individualist #Adjust Per Option
						has_ethic = ethic_fanatic_individualist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_collectivist #Adjust Per Option
						has_ethic = ethic_fanatic_collectivist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Spiritualist Reform
		name = bo_empire.2.spiritualist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_spiritualist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_SPIRITUALIST_ACCEPT_TOOLTIP
		make_country_more_spiritualist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_materialist #Adjust Per Option
							has_ethic = ethic_fanatic_materialist #Adjust Per Option
							has_ethic = ethic_spiritualist #Adjust Per Option
							has_ethic = ethic_fanatic_spiritualist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_materialist #Adjust Per Option
						has_ethic = ethic_fanatic_materialist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_spiritualist #Adjust Per Option
						has_ethic = ethic_fanatic_spiritualist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Materialist Reform
		name = bo_empire.2.materialist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_materialist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_MATERIALIST_ACCEPT_TOOLTIP
		make_country_more_materialist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_materialist #Adjust Per Option
							has_ethic = ethic_fanatic_materialist #Adjust Per Option
							has_ethic = ethic_spiritualist #Adjust Per Option
							has_ethic = ethic_fanatic_spiritualist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_spiritualist #Adjust Per Option
						has_ethic = ethic_fanatic_spiritualist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_materialist #Adjust Per Option
						has_ethic = ethic_fanatic_materialist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Xenophile Reform
		name = bo_empire.2.xenophile
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_xenophile #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_XENOPHILE_ACCEPT_TOOLTIP
		make_country_more_xenophile = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_xenophile #Adjust Per Option
							has_ethic = ethic_fanatic_xenophile #Adjust Per Option
							has_ethic = ethic_xenophobe #Adjust Per Option
							has_ethic = ethic_fanatic_xenophobe #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_xenophobe #Adjust Per Option
						has_ethic = ethic_fanatic_xenophobe #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_xenophile #Adjust Per Option
						has_ethic = ethic_fanatic_xenophile #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Xenophobe Reform
		name = bo_empire.2.xenophobe
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_xenophobe #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_XENOPHOBE_ACCEPT_TOOLTIP
		make_country_more_xenophobe = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_xenophile #Adjust Per Option
							has_ethic = ethic_fanatic_xenophile #Adjust Per Option
							has_ethic = ethic_xenophobe #Adjust Per Option
							has_ethic = ethic_fanatic_xenophobe #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_xenophile #Adjust Per Option
						has_ethic = ethic_fanatic_xenophile #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_xenophobe #Adjust Per Option
						has_ethic = ethic_fanatic_xenophobe #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Pacifist Reform
		name = bo_empire.2.pacifist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_pacifist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_PACIFIST_ACCEPT_TOOLTIP
		make_country_more_pacifist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_pacifist #Adjust Per Option
							has_ethic = ethic_fanatic_pacifist #Adjust Per Option
							has_ethic = ethic_militarist #Adjust Per Option
							has_ethic = ethic_fanatic_militarist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_militarist #Adjust Per Option
						has_ethic = ethic_fanatic_militarist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_pacifist #Adjust Per Option
						has_ethic = ethic_fanatic_pacifist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
	option = { #Militarist Reform
		name = bo_empire.2.militarist
		trigger = {
			NOT = { 
				has_ethic = ethic_fanatic_militarist #Adjust Per Option
			}
		}
		custom_tooltip = REFORM_MILITARIST_ACCEPT_TOOLTIP
		make_country_more_militarist = yes #Adjust Per Option
		hidden_effect = {
			every_owned_pop = {
				limit = {
					NOT = {
						OR = {
							has_ethic = ethic_pacifist #Adjust Per Option
							has_ethic = ethic_fanatic_pacifist #Adjust Per Option
							has_ethic = ethic_militarist #Adjust Per Option
							has_ethic = ethic_fanatic_militarist #Adjust Per Option
						}
					}
				}
				add_modifier = {
					modifier = "pop_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_pacifist #Adjust Per Option
						has_ethic = ethic_fanatic_pacifist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_very_upset_with_reform"
					days = 7300
				}
			}
			every_owned_pop = {
				limit = {
					OR = {
						has_ethic = ethic_militarist #Adjust Per Option
						has_ethic = ethic_fanatic_militarist #Adjust Per Option
					}
				}
				add_modifier = {
					modifier = "pop_happy_with_reform"
					days = 7300
				}
			}
		}
		
		ai_chance = {
			base = 1
		}
	}
	
}

#Coup Events Below

country_event = { #Coup Prepared
	id = bo_empire.10
	title = bo_empire.10.title
	desc = bo_empire.10.desc
	picture = GFX_evt_smugglers_in_bar
	
	trigger = {
		has_government = gov_imperial #Just to make sure...
		has_country_edict = stage_coup
		NOT = { has_country_flag = coup_prepared }
	}
	
	mean_time_to_happen = {
		months = 6
	}
	
	immediate = {
#		if = {
#			any_owned_leader = {
#				limit = {
#					has_skill = > 2
#					leader_class = admiral
#				}
#			}
#			random_owned_leader = {
#				limit = {
#					has_skill = > 2
#					leader_class = admiral
#				}
#				save_event_target_as = potential_wild_card
#			}
#		}
	}
	
	option = {
		name = bo_empire.10.a
		custom_tooltip = bo_empire.10.a.effect
		hidden_effect = {
			country_event = {
				id = bo_empire.11
				days = 12
				random = 8
			}
			set_country_flag = coup_prepared
		}
	}
	
	option = {
		name = bo_empire.10.b
	}
}

country_event = { #Coup Staged
	id = bo_empire.11
	title = bo_empire.11.title
	desc = bo_empire.11.desc
	picture = GFX_evt_smugglers_in_bar
	
	is_triggered_only = yes
	
	immediate = {
		
	}
	
	option = {
		name = bo_empire.11.storm_the_palace
		hidden_effect = { remove_country_flag = coup_prepared }
		add_modifier = {
			modifier = coup_in_progress
			days = -1
		}
		random_list = {
			50 = {
				custom_tooltip = bo_empire.11.coup_success
				hidden_effect = {
					country_event = {
						id = bo_empire.12
						days = 1
					}
				}
			}
			50 = {
				custom_tooltip = bo_empire.11.coup_problems
				hidden_effect = {
					country_event = {
						id = bo_empire.13
						days = 2
						random = 4
					}
				}
			}
		}
	}
}

country_event = { #Coup goes smoothly
	id = bo_empire.12
	title = bo_empire.12.title
	desc = bo_empire.12.desc
	picture = GFX_evt_throne_room
	
	is_triggered_only = yes
	
	immediate = {
		remove_modifier = coup_in_progress
		add_modifier = {
			modifier = recent_coup
			days = 1800
		}
	}
	
	option = {
		name = bo_empire.12.kill_ruler
		kill_leader = {
			type = ruler
			show_notification = yes
			heir = no
		}
		#This had better work...
	}
}

country_event = { #Coup runs into problems. This event decides what problem
	id = bo_empire.13
	title = bo_empire.13.title
	desc = bo_empire.13.desc
	picture = GFX_evt_interior_battle
	
	is_triggered_only = yes
	
	immediate = {
		random_list = {
			50 = {
				country_event = { #Heir is killed
					id = bo_empire.14
					days = 2
					random = 2
				}
			}
			50 = {
				country_event = { #Admiral resists
					id = bo_empire.15
					days = 3
					random = 6
				}
			}
		}
	}
	
	option = {
		name = bo_empire.13.not_good
	}
}

country_event = { #Heir is killed
	id = bo_empire.14
	title = bo_empire.14.title
	desc = bo_empire.14.desc
	picture = GFX_evt_interior_battle
	
	is_triggered_only = yes
	
	immediate = {
		remove_modifier = coup_in_progress
		add_modifier = {
			modifier = recent_coup
			days = 1800
		}
		kill_leader = {
			type = ruler
			show_notification = yes
			heir = yes
		}
		kill_leader = {
			type = ruler
			show_notification = yes
			heir = no
		}
		country_event = {
			id = pretender.1
			days = 1
		}
	}
	
	option = {
		name = bo_empire.14.kill_ruler
	}
}

country_event = { #Admiral Rebels
	id = bo_empire.15
	title = bo_empire.15.title
	desc = bo_empire.15.desc
	picture = GFX_evt_federation_fleet
	
	is_triggered_only = yes
	
	immediate = {
		remove_modifier = coup_in_progress
		add_modifier = {
			modifier = recent_coup
			days = 1800
		}
		kill_leader = {
			type = ruler
			show_notification = yes
			heir = no
		}
		if = {
			limit = {
				any_owned_leader = {
					leader_class = admiral
				}
			}
			random_owned_leader = {
				limit = {
					leader_class = admiral
				}
				save_event_target_as = rebel_admiral
			}
			else = {
				create_leader = {
					name = random
					species = root
					type = admiral
					skill = 1
					set_age = 50
#					traits = { }
				}
				last_created_leader = {
					save_event_target_as = rebel_admiral
				}
			}
		}
		event_target:rebel_admiral = {
			exile_leader_as = exiled_rebel_admiral
		}
		create_country = {
			name = "Loyalist Resistance"
			type = faction
			species = root
			flag = random
			effect = {
				save_event_target_as = resistance_country
				set_relation_flag = {
					who = root
					flag = coup_loyalists
				}
			}
		}
		random_owned_planet = {
			save_event_target_as = rebel_rally_planet
		}
		create_fleet = {
			name = "Loyalist Fleet"
			effect = {
				set_owner = root #It starts out belonging to the loyalists so that it can copy the loyalist ship designs
				set_location = event_target:rebel_rally_planet
				save_event_target_as = rebel_fleet
				set_fleet_stance = aggressive
			}
		}
		every_owned_pop = {
			random_list = {
				35 = {
					event_target:rebel_fleet = {
						spawn_defector_ships = yes
					}
				}
				65 = {
					#Do nothing
				}
			}
		}
		event_target:rebel_fleet = {
			set_owner = event_target:resistance_country
		}
	}
	
	option = {
		name = bo_empire.15.kill_ruler
	}
}

