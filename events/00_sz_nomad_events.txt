#################################
#
# Nomad Events
# by Niclas Karlsson & Maximilian Olbers
#
#################################

namespace = nomad

@tier1influencecontact = 7
@tier1influencecontactxenophile = 8
@tier1influencecontactmin = 20
@tier1influencecontactmax = 80

@tier2influencecontact = 10
@tier2influencecontactxenophile = 12
@tier2influencecontactmin = 40
@tier2influencecontactmax = 100

@tier1materialreward = 6
@tier1materialmin = 100
@tier1materialmax = 500
@tier2materialreward = 12
@tier2materialmin = 150
@tier2materialmax = 1000
@tier3materialreward = 18
@tier3materialmin = 250
@tier3materialmax = 1500
@tier4materialreward = 24
@tier4materialmin = 350
@tier4materialmax = 2000
@tier5materialreward = 48
@tier5materialmin = 700
@tier5materialmax = 4000

@tier1influencereward = 6
@tier1influencemin = 40
@tier1influencemax = 100
@tier2influencereward = 12
@tier2influencemin = 80
@tier2influencemax = 175
@tier3influencereward = 18
@tier3influencemin = 125
@tier3influencemax = 250
@tier4influencereward = 24
@tier4influencemin = 150
@tier4influencemax = 300
@tier5influencereward = 36
@tier5influencemin = 250
@tier5influencemax = 500
@tier6influencereward = 48
@tier6influencemin = 300
@tier6influencemax = 600

@tier1researchreward = 6
@tier1researchmin = 60
@tier1researchmax = 150
@tier2researchreward = 12
@tier2researchmin = 90
@tier2researchmax = 250
@tier3researchreward = 18
@tier3researchmin = 120
@tier3researchmax = 350
@tier4researchreward = 24
@tier4researchmin = 150
@tier4researchmax = 500
@tier5researchreward = 48
@tier5researchmin = 300
@tier5researchmax = 1000

#OVERWRITE

#Nomads contact planet owner and asks for colony access
country_event = {
	id = nomad.41
	title = nomad.10.name
	desc = nomad.41.desc
	diplomatic = yes
	is_triggered_only = yes
	
	picture_event_data = {
		portrait = event_target:contact_speaker_leader
		room = ethic_spaceship_room
	}

	trigger = {
		NOT = { has_country_flag = nomad_ignores }
		exists = from
		from = {
			owner = { is_country_type = nomad }
		}
	}

	immediate = {
		save_event_target_as = planet_owners
		event_target:nomad_fleet_country = {
			random_owned_ship = {
				fleet = {
					leader = { save_event_target_as = contact_speaker_leader }
					save_event_target_as = nomad_fleet
				}
			}
			owner_species = { save_event_target_as = nomad_fleet_species }
		}
		random_planet = {
			limit = {
				has_planet_flag = nomad_mission_location
			}
			save_event_target_as = NomadMissionPlanet
			remove_planet_flag = nomad_mission_location
		}
	}

	option = {
		name = nomad.41.a
		response_text = nomad.41.a.response
		custom_tooltip = nomad.41.a.tooltip
		add_monthly_resource_mult = {
			resource = influence
			value = @tier1influencereward
			min = @tier1influencemin
			max = @tier1influencemax
		}
		hidden_effect = {
			event_target:planet_owners = { set_timed_country_flag = { flag = day_0 days = 1 } }
			event_target:NomadMissionPlanet = {
				create_country = {
					name = random
					type = default
					species = Namarian
					ignore_initial_colony_error = yes
					#day_zero_contact = no
					#government = theocratic_republic
					government = gov_imperial
					flag = {
						icon = {
							category = "spherical"
							file = "flag_spherical_8.dds"
						}
						background= {
							category = "backgrounds"
							file = "00_solid.dds"
						}
						colors={
							"pink"
							"purple"
							"null"
							"null"
						}
					}
				}
				last_created_country = {
					establish_communications_no_message = event_target:planet_owners
					set_graphical_culture = mammalian_01
					set_country_flag = nomad02_country_flag
					add_energy = 1000
					add_minerals = 1000
					add_influence = 500
					#add_opinion_modifier = {
					#	who = event_target:planet_owners
					#	modifier = opinion_liberated_us
					#}
					add_opinion_modifier = {
						who = root
						modifier = opinion_shade_giver
					}
					if = {
						limit = {
							has_opinion_modifier = {
								who = event_target:planet_owners
								modifier = opinion_new_contact
							}
						}
						remove_opinion_modifier = {
							who = event_target:planet_owners
							modifier = opinion_new_contact
						}
					}
					event_target:planet_owners = {
						if = {
							limit = {
								has_opinion_modifier = {
									who = prev
									modifier = opinion_new_contact
								}
							}
							remove_opinion_modifier = {
								who = prev
								modifier = opinion_new_contact
							}
						}
					}
					save_event_target_as = nomad_new_country
				}
				create_colony = {
					owner = last_created_country
					species = Namarian
					ethos = {
						ethic = "ethic_xenophile"
						ethic = "ethic_spiritualist"
						ethic = "ethic_individualist"
					}
				}

				while = {
					count = 2
					best_tile_for_pop = {
						create_pop = {
							#species = event_target:nomad_fleet_species
							species = owner_main_species
							ethos = {
								ethic = "ethic_xenophile"
								ethic = "ethic_spiritualist"
								ethic = "ethic_individualist"
							}
						}
					}
				}
				set_owner = last_created_country
				event_target:nomad_new_country = {
					every_owned_pop = {
						modify_species = {
							species = prev
							ideal_planet_class = prevprev
						}
					}
				}
				surveyed = {
					set_surveyed = yes
					surveyor = root
				}
			}
			event_target:nomad_fleet = {
				owner = {
					set_country_flag = nomad_settled
					set_relation_flag = { who = event_target:planet_owners flag = nomad_relation_like }
				}
				remove_fleet_flag = nomad_on_mission
				queue_actions = {
					repeat = {
						max_iterations = 1
						wait = {
							duration = 30
							random = 5
						}
						effect = {
							id = nomad.41.waiting
							fleet_event = { id = nomad.2 }
						}
					}
				}
			}
		}
	}
	option = {
		name = nomad.41.b
		response_text = nomad.41.b.response
		custom_tooltip = nomad.41.b.tooltip
		trigger = {
			NOT = {
				has_ethic = ethic_fanatic_xenophobe
				has_ethic = ethic_xenophobe
				has_ethic = ethic_fanatic_spiritualist
				has_ethic = ethic_spiritualist
			}
		}
		add_influence = -50
		hidden_effect = {
			event_target:nomad_fleet = {
				remove_fleet_flag = nomad_on_mission
				fleet_event = { id = nomad.2 }
				owner = {
					set_relation_flag = { who = event_target:planet_owners flag = nomad_relation_dislike }
				}
			}
		}
	}
	option = {
		name = nomad.41.b.phobe
		response_text = nomad.41.b.response
		custom_tooltip = nomad.41.b.tooltip
		trigger = {
			OR = {
				has_ethic = ethic_fanatic_xenophobe
				has_ethic = ethic_xenophobe
			}
		}
		add_influence = -50
		hidden_effect = {
			event_target:nomad_fleet = {
				remove_fleet_flag = nomad_on_mission
				fleet_event = { id = nomad.2 }
				owner = {
					set_relation_flag = { who = event_target:planet_owners flag = nomad_relation_dislike }
				}
			}
		}
	}
	option = {
		name = nomad.41.b.spirit
		response_text = nomad.41.b.response
		custom_tooltip = nomad.41.b.tooltip
		trigger = {
			OR = {
				has_ethic = ethic_fanatic_spiritualist
				has_ethic = ethic_spiritualist
			}
			NOT = {
				has_ethic = ethic_fanatic_xenophobe
				has_ethic = ethic_xenophobe
			}
		}
		add_influence = -50
		hidden_effect = {
			event_target:nomad_fleet = {
				remove_fleet_flag = nomad_on_mission
				fleet_event = { id = nomad.2 }
				owner = {
					set_relation_flag = { who = event_target:planet_owners flag = nomad_relation_dislike }
				}
			}
		}
	}
	option = {
		name = nomad.41.c
		response_text = nomad.41.c.response
		is_dialog_only = yes
	}
}