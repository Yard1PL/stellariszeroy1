namespace = bo_faction

#Political Parties 
pop_faction_event = { #Individualist Reform
	id = bo_faction.1
	title = bo_faction.1.title
	desc = bo_faction.1.desc #bo_faction.1.desc
	picture = GFX_evt_galactic_senate
	
	is_triggered_only = yes
	
	immediate = {
		owner = { log = "Faction [Root.GetName] attempts reform in [This.GetName]" }
	}
	
	option = { #Individualist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_INDIVIDUALIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = individualist_party #Adjust Per Party
		}
		add_support = -1.0
		
		owner = {
			make_country_more_individualist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_individualist #Adjust Per Party
								has_ethic = ethic_fanatic_individualist #Adjust Per Party
								has_ethic = ethic_collectivist #Adjust Per Party
								has_ethic = ethic_fanatic_collectivist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_collectivist #Adjust Per Party
							has_ethic = ethic_fanatic_collectivist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_individualist #Adjust Per Party
							has_ethic = ethic_fanatic_individualist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Collectivist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_COLLECTIVIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = collectivist_party
		}
		add_support = -1.0
		owner = {
			make_country_more_collectivist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_individualist #Adjust Per Party
								has_ethic = ethic_fanatic_individualist #Adjust Per Party
								has_ethic = ethic_collectivist #Adjust Per Party
								has_ethic = ethic_fanatic_collectivist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_individualist #Adjust Per Party
							has_ethic = ethic_fanatic_individualist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_collectivist #Adjust Per Party
							has_ethic = ethic_fanatic_collectivist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Spiritualist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_SPIRITUALIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = spiritualist_party
		}
		add_support = -1.0
		owner = {
			make_country_more_spiritualist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_materialist #Adjust Per Party
								has_ethic = ethic_fanatic_materialist #Adjust Per Party
								has_ethic = ethic_spiritualist #Adjust Per Party
								has_ethic = ethic_fanatic_spiritualist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_materialist #Adjust Per Party
							has_ethic = ethic_fanatic_materialist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_spiritualist #Adjust Per Party
							has_ethic = ethic_fanatic_spiritualist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Materialist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_MATERIALIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = materialist_party
		}
		add_support = -1.0
		owner = {
			make_country_more_materialist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_materialist #Adjust Per Party
								has_ethic = ethic_fanatic_materialist #Adjust Per Party
								has_ethic = ethic_spiritualist #Adjust Per Party
								has_ethic = ethic_fanatic_spiritualist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_spiritualist #Adjust Per Party
							has_ethic = ethic_fanatic_spiritualist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_materialist #Adjust Per Party
							has_ethic = ethic_fanatic_materialist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Xenophile Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_XENOPHILE_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = xenophile_party
		}
		add_support = -1.0
		owner = {
			make_country_more_xenophile = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_xenophile #Adjust Per Party
								has_ethic = ethic_fanatic_xenophile #Adjust Per Party
								has_ethic = ethic_xenophobe #Adjust Per Party
								has_ethic = ethic_fanatic_xenophobe #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_xenophobe #Adjust Per Party
							has_ethic = ethic_fanatic_xenophobe #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_xenophile #Adjust Per Party
							has_ethic = ethic_fanatic_xenophile #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Xenophobe Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_XENOPHOBE_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = xenophobe_party
		}
		add_support = -1.0
		owner = {
			make_country_more_xenophobe = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_xenophile #Adjust Per Party
								has_ethic = ethic_fanatic_xenophile #Adjust Per Party
								has_ethic = ethic_xenophobe #Adjust Per Party
								has_ethic = ethic_fanatic_xenophobe #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_xenophile #Adjust Per Party
							has_ethic = ethic_fanatic_xenophile #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_xenophobe #Adjust Per Party
							has_ethic = ethic_fanatic_xenophobe #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Pacifist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_PACIFIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = pacifist_party
		}
		add_support = -1.0
		owner = {
			make_country_more_pacifist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_pacifist #Adjust Per Party
								has_ethic = ethic_fanatic_pacifist #Adjust Per Party
								has_ethic = ethic_militarist #Adjust Per Party
								has_ethic = ethic_fanatic_militarist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_militarist #Adjust Per Party
							has_ethic = ethic_fanatic_militarist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_pacifist #Adjust Per Party
							has_ethic = ethic_fanatic_pacifist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Militarist Reform
		name = ACCEPT_REFORM
		custom_tooltip = REFORM_MILITARIST_ACCEPT_TOOLTIP
		trigger = {
			is_pop_faction_type = militarist_party
		}
		add_support = -1.0
		owner = {
			make_country_more_militarist = yes #Adjust Per Party
			hidden_effect = {
				every_owned_pop = {
					limit = {
						NOT = {
							OR = {
								has_ethic = ethic_pacifist #Adjust Per Party
								has_ethic = ethic_fanatic_pacifist #Adjust Per Party
								has_ethic = ethic_militarist #Adjust Per Party
								has_ethic = ethic_fanatic_militarist #Adjust Per Party
							}
						}
					}
					add_modifier = {
						modifier = "pop_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_pacifist #Adjust Per Party
							has_ethic = ethic_fanatic_pacifist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_very_upset_with_reform"
						days = 7300
					}
				}
				every_owned_pop = {
					limit = {
						OR = {
							has_ethic = ethic_militarist #Adjust Per Party
							has_ethic = ethic_fanatic_militarist #Adjust Per Party
						}
					}
					add_modifier = {
						modifier = "pop_happy_with_reform"
						days = 7300
					}
				}
			}
		}
		
		ai_chance = {
			base = 1
			modifier = {
				factor = 4
				owner = { 
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	 
				}
			}			
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_05 }
			}
			modifier = {
				factor = 1.2
				owner = { has_policy_flag = democratic_tradition_04 }
			}			
		}
	}
	
	option = { #Refuse new reforms
		name = REFUSE_REFORM
		trigger = {
			NOT = { owner = { has_policy_flag = executive_legislative_power_split_pro_legislative } }
		}		
		add_support = 0.1
		custom_tooltip = REFUSE_REFORM_EFFECT
		custom_tooltip = DEMOCRATIC_TRADITION_DECREASED
		owner = {
			decrease_democratic_tradition = yes
		}
		log = "Reform vetoed"
		hidden_effect = {
			set_timed_pop_faction_flag = {
				flag = rejected_reform
				days = 7300
			}
			if = {
				limit = {
					owner = {
						exists = ruler
						ruler = {
							exists = pop_faction
							pop_faction = {
								NOT = { is_same_value = root }
							}
						}	
					}
				}
				switch = {
					trigger = is_pop_faction_type
					individualist_party = {
						owner.ruler = { set_leader_flag = angered_individualist_party }
					}
					collectivist_party = {
						owner.ruler = { set_leader_flag = angered_collectivist_party }
					}
					xenophobe_party = {
						owner.ruler = { set_leader_flag = angered_xenophobe_party }
					}
					xenophile_party = {
						owner.ruler = { set_leader_flag = angered_xenophile_party }
					}
					pacifist_party = {
						owner.ruler = { set_leader_flag = angered_pacifist_party }
					}
					militarist_party = {
						owner.ruler = { set_leader_flag = angered_militarist_party }
					}
					spiritualist_party = {
						owner.ruler = { set_leader_flag = angered_spiritualist_party }
					}
					materialist_party = {
						owner.ruler = { set_leader_flag = angered_materialist_party }
					}
				}				
			}							
		}
		
		ai_chance = {
			base = 3
			modifier = {
				factor = 1.5
				owner = { has_policy_flag = democratic_tradition_02 }
			}			
			modifier = {
				factor = 2
				owner = { has_policy_flag = democratic_tradition_01 }
			}
			modifier = {
				factor = 2
				exists = ruler
				ruler = {
					exists = pop_faction
					pop_faction = {
						OR = {
							AND = {
								is_pop_faction_type = individualist_party
								root = { is_pop_faction_type = collectivist_party }
							}
							AND = {
								is_pop_faction_type = collectivist_party
								root = { is_pop_faction_type = individualist_party }
							}
							AND = {
								is_pop_faction_type = militarist_party
								root = { is_pop_faction_type = pacifist_party }
							}
							AND = {
								is_pop_faction_type = pacifist_party
								root = { is_pop_faction_type = militarist_party }
							}
							AND = {
								is_pop_faction_type = xenophobe_party
								root = { is_pop_faction_type = xenophile_party }
							}
							AND = {
								is_pop_faction_type = xenophile_party
								root = { is_pop_faction_type = xenophobe_party }
							}
							AND = {
								is_pop_faction_type = materialist_party
								root = { is_pop_faction_type = spiritualist_party }
							}
							AND = {
								is_pop_faction_type = spiritualist_party
								root = { is_pop_faction_type = materialist_party }
							}
						}
					}
				}
			}			
			modifier = {
				factor = 0
				owner = {
					exists = ruler
					ruler = {
						exists = pop_faction
						pop_faction = {
							is_same_value = root
						}
					}	
				}
			}
		}
	}
}

pop_faction_event = { #Faction becoming rebellious
	id = bo_faction.2
	title = bo_faction.2.title
	desc = {
		text = "bo_faction.2.desc.parties"
		trigger = {
			OR = {
				is_pop_faction_type = individualist_party
				is_pop_faction_type = collectivist_party
				is_pop_faction_type = materialist_party
				is_pop_faction_type = spiritualist_party
				is_pop_faction_type = militarist_party
				is_pop_faction_type = pacifist_party
				is_pop_faction_type = xenophile_party
				is_pop_faction_type = xenophobe_party
			}
		}
	}
	desc = {
		text = "bo_faction.2.desc.heretics"
		trigger = {
			OR = {
				is_pop_faction_type = heretic_faction
			}
		}
	}
	picture = GFX_evt_smugglers_in_bar
	
	is_triggered_only = yes
	
	option = {
		name = UNFORTUNATE
		random_owned_pop = {
			limit = {
				NOT = {
					has_modifier = "pop_rebellious"
				}
			}
			add_modifier = {
				modifier = "pop_rebellious"
				days = 5475
			}
		}
		random_owned_pop = {
			limit = {
				NOT = {
					has_modifier = "pop_rebellious"
				}
			}
			add_modifier = {
				modifier = "pop_rebellious"
				days = 5475
			}
		}
		random_owned_pop = {
			limit = {
				NOT = {
					has_modifier = "pop_rebellious"
				}
			}
			add_modifier = {
				modifier = "pop_rebellious"
				days = 5475
			}
		}
	}
}

country_event = { #Removing reform denied flag 
	id = bo_faction.3
	hide_window = yes
	
	trigger = {
		has_government = gov_democracy
		exists = ruler
		ruler = {
			exists = pop_faction
		}
	}
	
	is_triggered_only = yes
	
	immediate = {
		ruler = {
			pop_faction = {
				remove_pop_faction_flag = rejected_reform
				remove_pop_faction_flag = rejected_policy_change
			}
		}
	}
}


#Corporations
planet_event = { #Corporation Founded
	id = bo_faction.101
	title = bo_faction.101.title
	desc = bo_faction.101.desc
	picture = GFX_evt_metropolis
	
	trigger = {
		has_owner = yes
		is_capital = no
		num_pops > 1
		owner = {
			OR = {
				has_ethic = ethic_individualist
				has_ethic = ethic_fanatic_individualist
			}
			NOT = {
				OR = {
					has_government = gov_mega_corporation
					has_government = gov_pirate_enclave
				}
			}
		}
		any_owned_pop = {
			can_join_factions = yes
		}
	}
	
	mean_time_to_happen = {
		years = 115
	}
	
	option = {
		name = OK
		random_owned_pop = {
			limit = {
				can_join_factions = yes
			}
			owner = {
				create_pop_faction = {
					type = corporation_faction
					parameter:empire = this
					parameter:home_planet = root
					pop = prev
				}
			}
		}
	}
}

#Revolutions
pop_faction_event = { #Revolution
	id = bo_faction.201
	title = bo_faction.201.title
	desc = bo_faction.201.desc
	picture = GFX_evt_ground_combat
	show_sound = event_planetary_riot

	is_triggered_only = yes

	option = {
		name = UNFORTUNATE
		hidden_effect = { kill_pop_faction = yes }
	}
}

pop_faction_event = { #Protest
	id = bo_faction.202
	title = bo_faction.202.title
	desc = {
		trigger = {
			owner = {
				has_policy_flag = protesting_legal
			}
		}
		text = "bo_faction.202.desc.a"
	}
	desc = {
		trigger = {
			owner = {
				has_policy_flag = protesting_illegal
			}
		}
		text = "bo_faction.202.desc.b"
	}
	picture = GFX_evt_city_ruins
	show_sound = event_planetary_riot
	
	is_triggered_only = yes
	
	option = {
		name = bo_faction.202.protesting_legal
		add_support = -0.1
		custom_tooltip = PROTEST_HAPPENS_EFFECT
		hidden_effect = {
			faction_protests = yes
		}
		trigger = {
			owner = {
				has_policy_flag = protesting_legal
			}
		}
		AI_chance = {
			base = 1
			modifier = {
				factor = 0
				NOT = {
					owner = {
						has_policy_flag = protesting_legal
					}
				}
			}
		}
	}
	
	option = {
		name = bo_faction.202.make_protesting_legal
		add_support = -0.2
		owner = {
			set_policy = {
				policy = peaceful_protesting
				option = protesting_legal
				cooldown = yes
			}
			custom_tooltip = AUTHORITY_DECREASED
#			decrease_authority = yes #Authority is decreased in the on_enabled for the policy, so this line was causing it do go down twice!
		}
		custom_tooltip = PROTEST_HAPPENS_EFFECT
		hidden_effect = {
			faction_protests = yes
		}
		trigger = {
			owner = {
				has_policy_flag = protesting_illegal
				NOT = { has_policy_flag = state_press } #"State Press" is mutually exclusive with "Protesting Legal".
			}
		}
		AI_chance = {
			base = 1
			modifier = {
				factor = 100
				owner = {
					OR = {
						has_policy_flag = authority_05
						has_policy_flag = authority_04
					}
				}
			}
			modifier = {
				factor = 0
				NOT = {
					owner = {
						has_policy_flag = protesting_illegal
						NOT = { has_policy_flag = state_press } #"State Press" is mutually exclusive with "Protesting Legal".
					}
				}
			}
		}
	}
	
	option = {
		name = bo_faction.202.ignore_protest
		add_support = -0.2
		custom_tooltip = PROTEST_HAPPENS_EFFECT
		hidden_effect = {
			faction_protests = yes
		}
		trigger = {
			owner = {
				has_policy_flag = protesting_illegal
			}
		}
		AI_chance = {
			base = 1
			modifier = {
				factor = 0
				NOT = {
					owner = {
						has_policy_flag = protesting_illegal
					}
				}
			}
		}
	}
	
	option = {
		name = bo_faction.202.send_the_military
		add_support = -0.5
		custom_tooltip = AUTHORITY_INCREASED
		owner = {
			add_modifier = {
				modifier = killed_protesters
				days = 3600
			}
			increase_authority = yes 
		}
		trigger = {
			owner = {
				has_policy_flag = protesting_illegal
				has_edict = martial_law
			}
		}
		AI_chance = {
			base = 1
			modifier = {
				factor = 0
				NOT = {
					owner = {
						has_policy_flag = protesting_illegal
						has_edict = martial_law
					}
				}
			}
		}
	}
}