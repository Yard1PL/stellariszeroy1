############################
#
# Advisor Events
#
# Written by Henrik Eklund
#
############################

namespace = advisor

#OVERWRITE

# Advisor Introduction
country_event = {
	id = advisor.1
	title = advisor.1.name
	
	# Generic Democratic Male
	desc = {
		text = advisor.1.desc_01
		show_sound = generic_democratic_male_intro # Good day, Mister President
		trigger = {
			has_government = gov_democracy
			NOT = { leader = { gender = female } }
		}
	}
	
	# Generic Democratic Female ## NO SUPPORT FOR FEMALES YET ##
	desc = {
		text = advisor.1.desc_02
		show_sound = generic_democratic_female_intro # Good day, Madame President
		trigger = {
			has_government = gov_democracy
			leader = { gender = female }
		}
	}
	
	# Generic Autocratic
	desc = {
		text = advisor.1.desc_04
		show_sound = generic_autocratic_intro# Your Imperial Majesty
		trigger = {
			has_government = gov_imperial
		}
	}
	
	is_advisor_event = yes
	is_triggered_only = yes
	
	trigger = {
		is_advisor_active = no
		is_tutorial_level > 0
	}
	
	immediate = {
		set_advisor_active = yes
		country_event = { id = advisor.26 days = 120 }
		capital_scope = {
			solar_system = {
				every_system_planet = {
					prevent_anomaly = yes
				}
				random_system_planet = {
					limit = {
						is_capital = no
						is_ringworld = no
						is_asteroid = no
						is_star = no
						NOT = { is_planet_class = pc_gas_giant }
					}
					prevent_anomaly = no
					add_anomaly = TUTORIAL_CAT
					set_planet_flag = tutorial_anomaly_planet
				}
			}
		}
	}
	
	option = {
		name = advisor.1.a
		custom_tooltip = full_tutorial
		hidden_effect = {
			country_event = { id = advisor.2 }
			set_tutorial_level = full
			set_country_flag = tutorial_level_picked
		}
	}
	option = {
		name = advisor.1.b
		custom_tooltip = tips_only
		hidden_effect = {
			country_event = { id = advisor.3 }
			set_tutorial_level = medium
			set_country_flag = tutorial_level_picked
		}
	}
	option = {
		name = advisor.1.c
		sound = "no_tut_advior"
		custom_tooltip = no_tutorial
		trigger = { has_country_flag = recycled_vir }
		hidden_effect = {
			set_tutorial_level = none
			set_country_flag = tutorial_level_picked
			capital_scope = {
				solar_system = {
					random_system_planet = {
						limit = {
							has_planet_flag = tutorial_anomaly_planet
						}
						prevent_anomaly = yes
						remove_planet_flag = tutorial_anomaly_planet
					}
				}
			}
		}
	}
	option = {
		name = advisor.1.c
		sound = "no_tut_advior"
		custom_tooltip = recycle_vir
		custom_tooltip = no_tutorial
		trigger = { NOT = { has_country_flag = recycled_vir } }
		hidden_effect = {
			add_minerals = 2
			set_tutorial_level = none
			set_country_flag = tutorial_level_picked
			capital_scope = {
				solar_system = {
					random_system_planet = {
						limit = {
							has_planet_flag = tutorial_anomaly_planet
						}
						prevent_anomaly = yes
						remove_planet_flag = tutorial_anomaly_planet
					}
				}
			}
		}
	}
}