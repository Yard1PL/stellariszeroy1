namespace = bo_country

country_event = { #Authority changing over time
	id = bo_country.1
	title = bo_country.1.title
	desc = bo_country.1.desc
	picture = GFX_evt_throne_room
	
	trigger = {
		is_country_type = default
		NOT = { has_country_flag = authority_being_changed }
	}
	
	mean_time_to_happen = {
		years = 15
	}
	
	option = {
		name = bo_country.1.increase
		custom_tooltip = bo_country.1.increase.effect
		hidden_effect = {
			country_event = {
				id = bo_notification.103
				days = 3600
				random = 1800
			}
			set_country_flag = authority_being_changed
		}
		trigger = {
			NOT = { has_policy_flag = authority_05 }
		}
		AI_chance = {
			base = 2
			modifier = {
				add = 2
				has_policy_flag = authority_01
			}
		}
	}
	
	option = {
		name = bo_country.1.status_quo
		AI_chance = {
			base = 3
		}
	}
	
	option = {
		name = bo_country.1.decrease
		custom_tooltip = bo_country.1.decrease.effect
		hidden_effect = {
			country_event = {
				id = bo_notification.104
				days = 3600
				random = 1800
			}
			set_country_flag = authority_being_changed
		}
		trigger = {
			NOT = { has_policy_flag = authority_01 }
		}
		AI_chance = {
			base = 2
			modifier = {
				add = 2
				has_policy_flag = authority_05
			}
		}
	}
}

country_event = { #Clean up heretics
	id = bo_country.2
	hide_window = yes
	
	is_triggered_only = yes
	
	immediate = {
		every_owned_pop = {
			limit = {
				has_modifier = pop_heretic
				OR = {
					NOT = {
						OR = {
							has_ethic = ethic_spiritualist
							has_ethic = ethic_fanatic_spiritualist
						}
					}
					#There have got to be more reasons for a pop to stop being a heretic than this...
				}
			}
			remove_modifier = pop_heretic
		}
	}
}

#country_event = {
#	id = bo_country.3
#	title = bo_country.3.title
#	desc = bo_country.3.desc
#	
#	is_triggered_only = yes
#	
#	option = {
#		name = YES
#		set_graphical_culture = "pirate_01"
#	}
#	
#	option = {
#		name = NO
#	}
#}

country_event = { #Pop seeks asylum in country
	id = bo_country.4
	title = bo_country.4.title
	desc = {
		text = bo_country.4.desc.not_pirates
		trigger = {
			NOT = { has_government = gov_pirate_enclave }
		}
	}
	desc = {
		text = bo_country.4.desc.pirates
		trigger = {
			has_government = gov_pirate_enclave
		}
	}
	picture = GFX_evt_unknown_ships
	
	is_triggered_only = yes
	
	immediate = {
		save_event_target_as = asylum_provider #Used by the notification events
		event_target:old_pop = {
			species = { save_event_target_as = old_pop_species }
			planet = { save_event_target_as = old_pop_planet }
		}
		random_owned_planet = {
			limit = {
				habitability = {
					who = event_target:old_pop
					value > 0.4
				}
				free_pop_tiles > 1 #I guess this could be > 0, but just to be extra safe.
			}
			save_event_target_as = migrate_destination_planet
		}
	}
	
	option = {
		name = bo_country.4.let_in
		custom_tooltip = bo_country.4.let_in.effect
		hidden_effect = {
			event_target:migrate_destination_planet = {
				create_pop = {
					species = event_target:old_pop
					ethos = root
				}
				if = {
					limit = {
						last_created_pop = {
							OR = {
								has_ethic = ethic_spiritualist 
								has_ethic = ethic_fanatic_spiritualist
							}
						}
						root = {
							OR = {
								has_ethic = ethic_spiritualist 
								has_ethic = ethic_fanatic_spiritualist
							}
							has_policy_flag = free_religion
						}
					}
					random_list = {
						33 = {
							last_created_pop = {
								pop_event = {
									id = bo_pop.5
								}
							}
						}
						66 = {
							#Nothing Happens
						}
					}
				}
			}
			if = {
				limit = {
					tech_unlocked_ratio = {
						who = event_target:fleeing_from
						ratio < 0.9
					}
				}
				country_event = {
					id = bo_notification.9
					days = 12
					random = 8
				}
			}
			event_target:fleeing_from = {
				country_event = {
					id = bo_notification.7
				}
			}
#			event_target:old_pop = { #This is done in the notification. If the pop is killed immediately the localisation can't reference them.
#				kill_pop = yes
#			}
			event_target:fleeing_from = {
				remove_opinion_modifier = {
					modifier = opinion_accepted_outlaws
					who = root
				}
				add_opinion_modifier = {
					modifier = opinion_accepted_outlaws
					who = root
				}
			}
		}
		AI_chance = {
			base = 1
		}
	}
	
	option = {
		name = bo_country.4.send_back
		trigger = {
			NOT = { has_government = gov_pirate_enclave }
		}
		custom_tooltip = bo_country.4.send_back.effect
		hidden_effect = {
			event_target:fleeing_from = {
				country_event = {
					id = bo_notification.8
				}
			}
			set_policy = {
				policy = asylum
				option = asylum_not_provided
				cooldown = yes
			}
		}
		AI_chance = {
			base = 0
			modifier = {
				add = 10
				AND = {
					OR = {
						is_friendly_to = event_target:fleeing_from
						is_threatened_to = event_target:fleeing_from
					}
					NOT = { has_ethic = ethic_fanatic_xenophile }
				}
			}
			modifier = {
				factor = 0
				has_government = gov_pirate_enclave
			}
		}
	}
}

country_event = { #Pirate Nation Forms
	id = bo_country.5
	title = bo_country.5.title
	desc = bo_country.5.desc
	picture = GFX_evt_exploding_ship
	
	is_triggered_only = yes
	
	trigger = {
		any_system = {
			any_planet = {
				is_surveyed = {
					who = root
					status = yes
				}
				habitability = {
					who = event_target:old_pop
					value > 0.4
				}
				has_owner = no
			}
		}
	}
	
	immediate = {
		random_planet = {
			limit = {
				is_surveyed = {
					who = root
					status = yes
				}
				habitability = {
					who = event_target:old_pop
					value > 0.4
				}
				has_owner = no
			}
			save_event_target_as = pirate_founding_planet
		}
		random_owned_planet = { #create_rebels must be in a planet scope, and I don't want to use create_country because it wouldn't copy the tech. So I create the rebels here and then assign them a planet later.
			create_rebels = {
				name = random
				government = gov_pirate_enclave
				species = event_target:old_pop.species
				ethos = {
					ethic = ethic_militarist
					ethic = ethic_individualist
				}
			}
		}
		last_created_country = {
			save_event_target_as = new_pirate_enclave
			declare_white_peace_with = root
			set_subject_of = {
				who = none
			}
			set_country_type = default
			country_event = {
				id = bo_arrg.0
				days = 1
			}
			create_leader = {
				type = ruler
				species = event_target:old_pop.species
				name = random
				skill = 0
				traits = {}
			}
			assign_leader = last_created_leader
		}
		event_target:pirate_founding_planet = {
			set_owner = event_target:new_pirate_enclave
			create_pop = { #Creates 3 pops to give them a fighting chance
				species = event_target:old_pop
				ethos = event_target:new_pirate_enclave
			}
			create_pop = {
				species = event_target:old_pop
				ethos = event_target:new_pirate_enclave
			}
			create_pop = {
				species = event_target:old_pop
				ethos = event_target:new_pirate_enclave
			}
			set_capital = yes
			create_spaceport = {
				owner = event_target:new_pirate_enclave
				initial_module = "projectile_weapon"
			}
		}
		event_target:new_pirate_enclave = {
			set_name = random #The name needs to be reset after a planet is assigned, since the naming uses the capital name.
		}
		while = { #Spawn Initial Fleet
			count = 6
			add_energy = 80
			add_minerals = 40
			create_fleet = {
				name = "Privateers"
				effect = {
					set_owner = event_target:new_pirate_enclave
					spawn_rebel_ships = yes
					set_location = {
						target = event_target:pirate_founding_planet
						distance = 35
						angle = random
					}
				}
			}
		}
		every_country = {
			limit = {
				event_target:pirate_founding_planet = {
					is_inside_border = prev
				}
			}
			country_event = {
				id = bo_notification.12
			}
		}
	}
	
	option = {
		name = bo_country.5.oh_no
	}
}

country_event = { #Force "stuck" rebel factions to revolt in ai empires.
	id = bo_country.101
	hide_window = yes
	
	trigger = {
		is_country_type = default
		is_ai = yes
	}
	
	mean_time_to_happen = {
		months = 2
	}
	
	immediate = {
		set_variable = {
			which = total_pops_in_empire
			value = 0
		}
		every_owned_pop = {
			root = {
				change_variable = {
					which = total_pops_in_empire
					value = 1
				}
			}
		}
		every_pop_faction = {
			limit = {
				OR = {
					is_pop_faction_type = revolutionaries
					is_pop_faction_type = sector_separatists
					is_pop_faction_type = malcontent_slaves
				}
			}
			root = {
				set_variable = {
					which = revolt_requirement
					value = total_pops_in_empire
				}
			}
			
			if = {
				limit = {
					is_pop_faction_type = revolutionaries
				}
				root = {
					multiply_variable = {
						which = revolt_requirement
						value = 0.35
					}
				}
			}
			if = {
				limit = {
					is_pop_faction_type = sector_separatists
				}
				root = {
					multiply_variable = {
						which = revolt_requirement
						value = 0.20
					}
				}
			}
			if = {
				limit = {
					is_pop_faction_type = malcontent_slaves
				}
				root = {
					multiply_variable = {
						which = revolt_requirement
						value = 0.45
					}
				}
			}
			
			root = {
				set_variable = {
					which = total_pops_in_faction
					value = 0
				}
			}
			every_owned_pop = {
				root = {
					change_variable = {
						which = total_pops_in_faction
						value = 1
					}
				}
			}
			if = {
				limit = {
					root = {
						check_variable = {
							which = total_pops_in_faction
							value > revolt_requirement
						}
					}
				}
				add_support = 0.1
			}
		}
	}
}